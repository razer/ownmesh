from __future__ import absolute_import
import logging
from celery import task

logger = logging.getLogger(__name__)


@task(queue='services')
def check_services_task(init=False):
    from core.hosts.tasks import check_connector_task
    check_connector_task(init=init)

    from core.hosts.local.gpio.tasks import (
        check_service_task as gpio_check_service_task)
    gpio_check_service_task(init=init)
