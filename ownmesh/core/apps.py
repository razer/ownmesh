from django.apps import AppConfig

import logging
logger = logging.getLogger(__name__)


class CoreConfig(AppConfig):
    name = 'core'

    def ready(self):
        logger.debug('Core app signals init')
        from core import signals as coresignals  # @UnusedImport
