import logging
import time
from ast import literal_eval

from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.core.exceptions import ValidationError
from django.utils import timezone

from project.settings import localtime
from values import datas as V
from values.hosts import FRAME as F
# from values.colors import guicolor
from core.hosts.models import Host
from core.hosts.config import LOCAL
from .validators import validate_header

logger = logging.getLogger(__name__)


class Data(models.Model):
    host = models.ForeignKey(Host, related_name='data',
                             on_delete=models.CASCADE,
                             help_text=_('The host the data is related'))
    header = models.TextField(default='00', max_length=2,
                              validators=[validate_header],
                              help_text=_('The data identifier : must be a ' +
                                          'byte in hex form (ex: A5, 9B...)'))
    shortname = models.TextField(default='Unnamed data', max_length=20)
    name = models.TextField(default='Unnamed data', max_length=40)
    role = models.CharField(max_length=2,
                            choices=V.ROLE_CHOICES,
                            default=V.NONE)
    unit = models.TextField(default='', blank=True, max_length=4,
                            help_text=_('Unit of the data : max 4 caracters'))
    # value_limit = models.ForeignKey(ValueLimit, null=True, blank=True)
    value_min = models.FloatField(default=None, blank=True, null=True,
                                  help_text=_('Value limit low'))
    value_max = models.FloatField(default=None, blank=True, null=True,
                                  help_text=_('Value limit high'))
    value_step = models.FloatField(default=1, blank=True,
                                   help_text=_('Value edit step'))
    # RO(readonly) / WO(writeonly) / RW(readwrite)
    mode = models.CharField(
        max_length=2, choices=V.MODE_CHOICES, default=V.READWRITE)
    enabled = models.BooleanField(default=True)
    visible = models.BooleanField(default=True)
    entry_type = models.CharField(max_length=2,
                                  choices=V.TYPE_CHOICES,
                                  default=V.INT,
                                  help_text=_(
                                      'Bool (True/False), ' +
                                      'Integer (-32000 to 32000), ' +
                                      'Float (-32000.999 to 32000.999), ' +
                                      'String (max 10 caracters)'))
    entry_creation = models.CharField(
        max_length=2, choices=V.ENTRY_CHOICES, default=V.ALWAYS,
        help_text=_('Always save entry: new entry is always created on new ' +
                    'value; Create entry when value change: new entry is ' +
                    'created only if the new value different than the ' +
                    'previous one; Always edit previous entry: the previous ' +
                    'entry is edited with the new value, no history (no ' +
                    'charts) '))
    always_save_entry = models.BooleanField(
        default=False, help_text=_('If entry is only created when value ' +
                                   'change, ensure that previous entry is ' +
                                   'saved, otherwise actions are not ' +
                                   'triggered'))
    pending = models.BooleanField(default=False)
    on_alarm = models.BooleanField(default=False)
    keep_days = models.IntegerField(default=0)
    added_on = models.DateTimeField(default=timezone.now, blank=True)
    edited_on = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return (f'{self.host.name}|{self.shortname}|{self.header}|' +
                f'{self.get_entry_type_display()}|{self.mode}')

    def clean(self):
        if len(self.header) == 1:
            self.header = '0{}'.format(self.header)
        self.header = self.header.lower()
        if self.host.get_type() != LOCAL:
            data_filter = Data.objects.filter(host=self.host)
            for data in data_filter.exclude(pk=self.pk):
                if data.header == self.header:
                    raise ValidationError(
                        _('header %(header)s already used by %(dataname)s'),
                        params={'header': self.header, 'dataname': data.name})
        super(Data, self).clean()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.clean()
        self.edited_on = timezone.now()
        if self.entry_type not in V.BOOL and self.entry_type not in V.STRING:
            if self.value_min is None:
                self.value_min = -32000
            if self.value_max is None:
                self.value_max = 32000
        super(Data, self).save()

    @property
    def entries(self):
        return getattr(self, f'{self.entry_type.lower()}entries')

    @property
    def latest_entry(self):
        entries = self.entries
        if not entries:
            return None
        if not entries.count():
            return None
        return entries.latest('pub_date')

    @property
    def latest_value(self):
        latest_entry = self.latest_entry
        if not latest_entry:
            return None
        return latest_entry.value

    def get_type(self):
        for model in 'alarm', 'netstatus':
            if hasattr(self, model):
                return model
        return None

    def serialize(self):
        latest_value = self.latest_value
        if latest_value is None:
            return None
        if isinstance(latest_value, str):  # type(latest_value) is str:
            value = latest_value
        elif isinstance(latest_value, bool):  # type(latest_value) is bool:
            value = str(int(latest_value))
        else:
            value = str(latest_value)
        return F.DELIMIT + bytes(self.header + value, 'utf-8')

    def set_value(self, value, direction=V.TRANSMITTED, run_actions=True):
        def _edit_latestentry(value, direction, run_actions):
            e = self.latest_entry
            if e:
                e.value = value
                e.direction = direction
                e.run_actions = run_actions
                e.pub_date = timezone.now()
                e.save()
                return True
            return None

        logstart = (f'Set data {self.entry_type}|{self.header}|' +
                    f'{self.name}|{self.host.name}')

        if V.BOOL in self.entry_type:
            if isinstance(value, str):
                value = bytes(value, 'utf-8')
            if isinstance(value, bytes):
                value = bool(literal_eval(value.decode('utf-8').capitalize()))
            else:
                value = bool(value)
        elif V.INT in self.entry_type:
            value = int(float(value))
        elif V.FLOAT in self.entry_type:
            value = float(value)

        if self.entry_type not in V.BOOL and self.entry_type not in V.STRING:
            if value > self.value_max or value < self.value_min:
                if V.TRANSMITTED in direction:
                    err_msg = (f'{value} : outside limits : ' +
                               f'{self.value_min}/{self.value_max}')
                    logger.error(err_msg)
                    raise ValueError(err_msg)
                else:
                    # TODO: Handle alarms Here !
                    pass

        if V.ONLYNEWVALUE in self.entry_creation:
            latest_entry = self.latest_entry
            if latest_entry:
                oldvalue = latest_entry.value
                if oldvalue == value:
                    logger.info(f'{logstart} to {value} : ONLYNEWVALUE, ' +
                                f'same value ({oldvalue})')
                    if self.always_save_entry:
                        _edit_latestentry(value, direction, run_actions)
                    return

        if V.NOPERSISTANCE in self.entry_creation:
            logger.info(f'{logstart} to {value} : NOPERSISTANCE')
            if _edit_latestentry(value, direction, run_actions):
                return

        logger.info(f'{logstart} to {value} : creating new entry')
        self.entries.create(
            direction=direction, value=value,
            run_actions=run_actions)

    def set_pending(self, pending):
        latest_entry = self.latest_entry
        if not latest_entry:
            return None
        if latest_entry.direction == V.RECEIVED:
            return False
        if self.mode == V.READONLY:
            return False
        if self.host.get_type() == LOCAL:
            return False
        if self.pending != pending:
            self.pending = pending
            self.save()
            return True
        return False

    def run_actions(self):
        entry = self.latest_entry
        if not entry.run_actions:
            logger.info(
                f'Action trig disabled by entry,id {entry.pk} for {self.name}')
            return
        if not self.actiontrigger.count():
            return
        for actiontrigger in self.actiontrigger.all():
            actiontrigger.action.run(trigger=actiontrigger)
        return


class Alarm(Data):
    related_data = models.ForeignKey(Data, related_name='alarm_data',
                                     on_delete=models.CASCADE)
    bypass = models.BooleanField(default=False)
    bypass_until = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        host = None
        for host in Host.objects.all():
            if host.get_type() in LOCAL:
                break
        assert host is not None, 'Local host not found !'
        assert host.get_type() in LOCAL, 'Local host not found !'
        self.host = host
        self.header = '00'
        self.name = (f'Alarm for {self.related_data.name} '
                     + f'({self.related_data.host.name})')
        self.shortname = (f'Alarm {self.related_data.shortname} '
                          + f'({self.related_data.host.name})')
        self.role = V.ALARM
        self.entry_type = V.BOOL
        self.entry_creation = V.ONLYNEWVALUE
        super(Alarm, self).save()


class NetStatus(Data):
    related_host = models.OneToOneField(Host, related_name='netstatus_data',
                                        on_delete=models.CASCADE)
    wait_response = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Entry(models.Model):
    class Meta:
        abstract = True
        ordering = ['-pub_date']

    direction = models.CharField(max_length=2,
                                 choices=V.REMOTE_CHOICES,
                                 default=V.RECEIVED)
    run_actions = models.BooleanField(default=True)
    pub_date = models.DateTimeField(default=timezone.now, blank=True)

    @property
    def human_pubdate(self):
        return localtime(self.pub_date).strftime("%d/%m %H:%M:%S")

    @property
    def unix_pubdate(self):
        return time.mktime(localtime(self.pub_date).timetuple()) * 1000

    def __str__(self):              # __unicode__ on Python 2
        return (f'{self.human_pubdate}|{self.data.shortname}|' +
                f'{self.get_direction_display()}|{str(self.value)}')


class BoEntry(Entry):
    data = models.ForeignKey(Data, related_name='boentries',
                             on_delete=models.CASCADE)
    value = models.BooleanField(default=False)


class ItEntry(Entry):
    data = models.ForeignKey(Data, related_name='itentries',
                             on_delete=models.CASCADE)
    value = models.IntegerField(default=0)


class FlEntry(Entry):
    data = models.ForeignKey(Data, related_name='flentries',
                             on_delete=models.CASCADE)
    value = models.FloatField(default=0)


class StEntry(Entry):
    data = models.ForeignKey(Data, related_name='stentries',
                             on_delete=models.CASCADE)
    value = models.TextField(default='empty', max_length=30)
