import logging
from django.contrib import admin
from django import urls
from django.utils.translation import ugettext_lazy as _
from project.admin import RulesAdmin
from values import datas as V
from .models import (Data, Alarm, NetStatus, BoEntry, FlEntry,
                     StEntry, ItEntry)

logger = logging.getLogger(__name__)


@admin.register(Data)
class DataAdmin(RulesAdmin):
    list_display = ('name', 'link_to_host', 'entry_type',
                    'link_to_entries', 'pending')  # , 'header')
    list_filter = ('name', 'host', 'pending')

    def link_to_entries(self, obj):
        entry_type = f'{obj.entry_type.lower()}entry'
        entry_filter = f'?data__id__exact={obj.pk}'
        link = urls.reverse(f'admin:datas_{entry_type}_changelist')
        return f'<a href="{link}{entry_filter}">Entries</a>'
    link_to_entries.allow_tags = True
    link_to_entries.short_description = 'Entry list'

    def link_to_host(self, obj):
        host_type = obj.host.get_type()
        link = urls.reverse(f'admin:{host_type}_{host_type}_change',
                            args=[obj.host.id])
        return f'<a href="{link}">{obj.host.name}</a>'
    link_to_host.allow_tags = True
    link_to_host.admin_order_field = 'host__name'
    link_to_host.short_description = 'Host'

    def get_fieldsets(self, request, obj=None):
        if obj is not None:
            self.fieldsets = (
                (None, {
                    'fields': ('host', 'header', 'shortname', 'name',
                               'mode')
                }),
                (_('Entries options'), {
                    'classes': ('wide',),
                    'fields': ('entry_type', 'value_min', 'value_max',
                               'value_step', 'entry_creation',
                               'always_save_entry', 'keep_days')
                }),
                (_('Advanced options'), {
                    'classes': ('wide',),
                    'fields': ('unit', 'pending', 'enabled', 'visible')
                }),
            )
            return self.fieldsets
        return super(DataAdmin, self).get_fieldsets(request, obj)

    def get_readonly_fields(self, request, obj=None):
        '''
        Override to make certain fields readonly if this is a change request
        '''
        if obj is not None:
            return self.readonly_fields + ('link_to_host', 'entry_type',
                                           'pending')
        return self.readonly_fields

    def get_queryset(self, request):
        qs = super(DataAdmin, self).get_queryset(request)
        return qs.filter(role=V.NONE)


@admin.register(BoEntry, FlEntry, ItEntry, StEntry)
class EntryAdmin(admin.ModelAdmin):
    list_display = ('date', 'related_data', 'related_host', 'entry_type',
                    'direction', 'value')
    date_hierarchy = 'pub_date'
    fields = ('data', 'value')

    def date(self, obj):
        return obj.human_pubdate

    def related_data(self, obj):
        return obj.data.shortname

    def related_host(self, obj):
        return obj.data.host.name

    def entry_type(self, obj):
        return obj.data.entry_type

    def value(self, obj):
        return str(obj.value) + obj.data.unit

    related_data.admin_order_field = 'data__shortname'
    entry_type.admin_order_field = 'data__entry_type'
#     value.admin_order_field = 'data__entry_type'

    def get_queryset(self, request):
        qs = super(EntryAdmin, self).get_queryset(request)
        return qs.filter(data__role=V.NONE)


admin.site.register(Alarm)
admin.site.register(NetStatus)
# admin.site.register(ValueLimit)
