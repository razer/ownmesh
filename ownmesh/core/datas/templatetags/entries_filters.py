from django.template import Library
register = Library()


@register.filter(name='model_is_entry')
def model_is_entry(model):
    return ('entr' in model)
