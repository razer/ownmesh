import logging
from celery import task
from core.datas.models import Data
# from core.hosts.local.models import Local
from values.datas import BOOL, STRING, ALARM, WRITEONLY

logger = logging.getLogger(__name__)


@task(queue='services')
def run_actions_task(pk):
    if pk is None:
        return
    data_filter = Data.objects.filter(pk=pk)
    if not data_filter.count():
        return
    data = data_filter.first()
    data.run_actions()


@task(queue='services')
def check_alarm_task(pk, value=None):
    data = Data.objects.get(pk=pk)
    if data.mode == WRITEONLY or data.role == ALARM:
        return
    if data.entry_type == BOOL or data.entry_type == STRING:
        return
    alarm_data, _ = data.alarm_data.get_or_create()
    alarm = False
    if value is None:
        data_value = data.latest_value
        if data_value is None:
            return
        value = data_value
    # logger.info(f'check_alarm_task : alarm_data {alarm_data}, value {value}')
    # logger.info(f'value_min {data.value_min}, value_max {data.value_max}')
    if value > data.value_max or value < data.value_min:
        alarm = True
    if alarm_data.latest_value is None or alarm_data.latest_value is not alarm:
        alarm_data.set_value(alarm)
        data.on_alarm = alarm
        data.save()
