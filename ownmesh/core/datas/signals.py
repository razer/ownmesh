import logging
from django.db.models.signals import post_save  # , pre_delete
from django.dispatch import receiver
from api.serializers import EntrySerializer, DataSerializer  # , HostSerializer
from core.hosts.local.models import Local
from core.hosts.tasks import host_set_values_task
from core.hosts.local.gpio.tasks import gpio_output_task
from core.datas.models import Data, BoEntry, FlEntry, ItEntry, StEntry
from core.datas.tasks import run_actions_task, check_alarm_task
from frontend.consumers import sse_push
from frontend.events.tasks import refresh_eventcache_task
from values.local import DISABLED
import values.datas as VD

logger = logging.getLogger(__name__)


@receiver(post_save, sender=BoEntry)
@receiver(post_save, sender=FlEntry)
@receiver(post_save, sender=ItEntry)
@receiver(post_save, sender=StEntry)
def on_entry_saved(sender, **kwargs):
    entry = kwargs['instance']
    data = entry.data
    host = entry.data.host
    local = Local.objects.first()
    # Send server event
    sse_push('lastentry', EntrySerializer(entry).data)

    # Refresh events
    refresh_eventcache_task.delay(data.entry_type, entry.pk)

    # Check alarm for limits
    check_alarm_task.delay(data.pk, entry.value)

    # Run the actions
    run_actions_task.s(data.pk).delay()

    if host.pk is local.pk:
        # Netstatus data entry : init on new connection
        if data.role == VD.NETSTATUS:
            if entry.value:
                host_set_values_task.delay(pk=data.related_host.pk,
                                           clock=True,
                                           onlypending=False)
            return
        # Local specific (GPIO)
        if local.gpio == DISABLED:
            return
        if hasattr(data, 'iopin'):
            gpio_output_task.delay(pin=data.iopin.pin)
            return

    # Set pending state for related data
    data.set_pending(True)
    # Trigger to set new values to remote host
    host_set_values_task.delay(pk=host.pk)


@receiver(post_save, sender=Data)
def on_data_saved(sender, **kwargs):
    data = kwargs['instance']
    # logger.info(f'Signal post_saved from Data {data.name}')

    # Check alarm for limits (delay ensure db committing, keep it)
    check_alarm_task.s(data.pk).apply_async(countdown=3)
    sse_push('data', DataSerializer(data).data)
