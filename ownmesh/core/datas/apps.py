import logging
from django.apps import AppConfig

logger = logging.getLogger(__name__)


class DataConfig(AppConfig):
    name = 'core.datas'

    def ready(self):
        logger.debug('Data app signals init')
        # flake8: noqa
        from . import signals as datasignals  # pylint: disable=W0612
