from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


def validate_header(value):
    try:
        valuehex = int('0x{}'.format(value), 16)  # @UnusedVariable
    except ValueError:
        print('Value error')
        raise ValidationError(
            _('%(value)s is not an hex number'),
            params={'value': value},
        )
