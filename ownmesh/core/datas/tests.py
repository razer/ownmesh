from django.test import TestCase
from django.core.exceptions import ValidationError
from .models import Data
from values import datas as V
from core.hosts.models import Host
from core.hosts.local.models import Local


class DataTestCase(TestCase):
    def setUp(self):
        self.localhost = Local.objects.create(name='DataTestLocalHost')
        self.remotehost = Host.objects.create(name='DataTestRemoteHost')

    def test_values(self):
        ''' Data and entries tests'''
        msg = 'Data creation'
        Data.objects.create(host=self.localhost, name="test bool data 1",
                            shortname="testbodata1",
                            header='01',
                            entry_type=V.BOOL)
        # 2 datas since netstatus data should be created for remote host
        self.assertEqual(Data.objects.count(), 2, msg=msg)
        Data.objects.create(host=self.localhost, name="test int data 1",
                            shortname="testitdata1",
                            header='02',
                            entry_type=V.INT)
        self.assertEqual(Data.objects.count(), 3)

        msg = 'Data Header can\'t be the same on several instances'
        Data.objects.create(host=self.remotehost,
                            name="test bool remote data 1",
                            shortname="testrembodata2",
                            header='01',
                            entry_type=V.BOOL)
        with self.assertRaises(ValidationError, msg=msg):
            bad_data = Data.objects.create(host=self.remotehost,
                                           name="test bool data 1",
                                           shortname="testbodata1",
                                           header='01',
                                           entry_type=V.BOOL)
            bad_data.full_clean()

        msg = 'Set values / Get latest value'
        bo_data = Data.objects.get(shortname="testbodata1")
        self.assertEqual(bo_data.set_value(True), True, msg=msg)
        self.assertEqual(bo_data.entries.count(), 1, msg=msg)
        self.assertEqual(bo_data.latest_value, True, msg=msg)
        self.assertEqual(bo_data.set_value(False), True, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        self.assertEqual(bo_data.entries.count(), 2, msg=msg)

        msg = 'Entry creation logic'
        bo_data.set_value(True)
        self.assertEqual(bo_data.entries.count(), 3, msg=msg)
        bo_data.entry_creation = V.ALWAYS
        bo_data.save()
        bo_data.set_value(True)
        self.assertEqual(bo_data.entries.count(), 4, msg=msg)
        bo_data.entry_creation = V.NOPERSISTANCE
        bo_data.save()
        bo_data.set_value(False)
        self.assertEqual(bo_data.entries.count(), 4, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        for e in bo_data.entries.all():
            e.delete()
        bo_data.set_value(False)
        self.assertEqual(bo_data.entries.count(), 1, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)

        msg = 'Boolean Entry value conversion'
        self.assertEqual(bo_data.set_value(0), True, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        self.assertEqual(bo_data.set_value(1), True, msg=msg)
        self.assertEqual(bo_data.latest_value, True, msg=msg)
        self.assertEqual(bo_data.set_value(100), True, msg=msg)
        self.assertEqual(bo_data.latest_value, True, msg=msg)
        self.assertEqual(bo_data.set_value('0'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        self.assertEqual(bo_data.set_value('1'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, True, msg=msg)
        self.assertEqual(bo_data.set_value('000'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        self.assertEqual(bo_data.set_value('100'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, True, msg=msg)
        self.assertEqual(bo_data.set_value('False'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        self.assertEqual(bo_data.set_value('True'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, True, msg=msg)
        self.assertEqual(bo_data.set_value('false'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        self.assertEqual(bo_data.set_value('true'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, True, msg=msg)
        self.assertEqual(bo_data.set_value(b'0'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        self.assertEqual(bo_data.set_value(b'1'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, True, msg=msg)
        self.assertEqual(bo_data.set_value(b'000'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        self.assertEqual(bo_data.set_value(b'100'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, True, msg=msg)
        self.assertEqual(bo_data.set_value(b'False'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        self.assertEqual(bo_data.set_value(b'True'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, True, msg=msg)
        self.assertEqual(bo_data.set_value(b'false'), True, msg=msg)
        self.assertEqual(bo_data.latest_value, False, msg=msg)
        self.assertEqual(bo_data.set_value(b'true'), True, msg=msg)

        msg = 'Integer Entry value conversion'
        it_data = Data.objects.get(shortname="testitdata1")
        self.assertEqual(it_data.set_value(150), True, msg=msg)
        self.assertEqual(it_data.entries.count(), 1, msg=msg)
        self.assertEqual(it_data.latest_value, 150, msg=msg)
        self.assertEqual(it_data.set_value(-600), True, msg=msg)
        self.assertEqual(it_data.latest_value, -600, msg=msg)
        self.assertEqual(it_data.set_value('150'), True, msg=msg)
        self.assertEqual(it_data.latest_value, 150)
        self.assertEqual(it_data.set_value('-600'), True, msg=msg)
        self.assertEqual(it_data.latest_value, -600, msg=msg)
        self.assertEqual(it_data.set_value(b'150'), True, msg=msg)
        self.assertEqual(it_data.latest_value, 150, msg=msg)
        self.assertEqual(it_data.set_value(b'-600'), True, msg=msg)
        self.assertEqual(it_data.latest_value, -600, msg=msg)
