import logging
from django.contrib import admin
from django import urls
from project.admin import RulesAdmin
from .models import Host

logger = logging.getLogger(__name__)


class BaseHostAdmin(RulesAdmin):
    fields = ('name', 'guicolor', 'power_mode')
    plugin_fields = ()
    plugin_readonly_fields = ('edited_on')

    def get_fields(self, request, obj=None):
        if obj is not None:
            if 'enabled' not in self.fields:
                self.fields = self.fields + ('enabled',)
        if hasattr(self, 'plugin_fields'):
            for field in self.plugin_fields:
                if field not in self.fields:
                    self.fields = self.fields + (field,)
        return RulesAdmin.get_fields(self, request, obj=obj)

    def get_readonly_fields(self, request, obj=None):
        if hasattr(self, 'plugin_readonly_fields'):
            for field in self.plugin_readonly_fields:
                if field not in self.readonly_fields:
                    self.readonly_fields = self.readonly_fields + (field,)
        return RulesAdmin.get_readonly_fields(self, request, obj=obj)


class HostPluginConfigAdmin(RulesAdmin):
    fields = ('enabled', 'state', 'error')
    readonly_fields = ('state', 'error')
    plugin_fields = ()
    plugin_readonly_fields = ()

    def state(self, obj):
        if not obj:
            return None
        return obj.status.get_state_display()

    def error(self, obj):
        if not obj:
            return None
        return obj.status.error

    def has_add_permission(self, request):
        if self.model.objects.count():
            return False
        else:
            super().has_add_permission(request)

    def get_fields(self, request, obj=None):
        if hasattr(self, 'plugin_fields'):
            for field in self.plugin_fields:
                if field not in self.fields:
                    self.fields = self.fields + (field,)
        return RulesAdmin.get_fields(self, request, obj=obj)

    def get_readonly_fields(self, request, obj=None):
        if hasattr(self, 'plugin_readonly_fields'):
            for field in self.plugin_readonly_fields:
                if field not in self.readonly_fields:
                    self.readonly_fields = self.readonly_fields + (field,)
        return RulesAdmin.get_readonly_fields(self, request, obj=obj)


@admin.register(Host)
class HostAdmin(BaseHostAdmin):
    list_display = ('name', 'host_type', 'power_mode', 'online', 'enabled',
                    'edited_on')

    def host_type(self, obj):
        return obj.get_type()

    def host_name(self, obj):
        host_type = obj.get_type()
        link = urls.reverse(f'admin:{host_type}_{host_type}_change',
                            args=[obj.id])
        return u'<a href="%s">%s</a>' % (link, obj.name)
    host_name.allow_tags = True
    host_name.admin_order_field = 'name'
    host_name.short_description = 'Name'

    def online(self, obj):
        return obj.online
    online.boolean = True
