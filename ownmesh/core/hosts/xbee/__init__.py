from __future__ import absolute_import

from core.hosts.config import HOSTTYPES, XBEE
import logging
logger = logging.getLogger(__name__)

default_app_config = 'core.hosts.xbee.apps.XbeeConfig'

ZigBee = None  # @UnusedVariable
Serial = None  # @UnusedVariable
Dispatch = None  # @UnusedVariable

hexdelimiter = ':'  # @UnusedVariable

try:
    from xbee import ZigBee
    from serial import Serial
    from xbee.helpers.dispatch import Dispatch
except Exception:
    if XBEE in HOSTTYPES:
        logger.error(
            'No library found for Xbee radio modules, support is disabled')
        logger.error('Please install python Xbee bindings')
