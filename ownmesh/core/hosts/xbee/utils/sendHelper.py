from .dataConvert import strToByte
from .frameHelper import AtCommand, PinMode

import logging
logger = logging.getLogger(__name__)

EMPTYBYTE = b''


class SendHelper:
    """
    Provides a OwnMesh specific backend to python-xbee zigbee class
    each method should be called with argument format :
                    'frame_id'=str,     - optional frame ID
                    'address'=int16     - address type long of zigbee
                    'set_data'=str      - optional set_data content
                    'pin'=str           - pin (D0->D7) for io command
                    'mode'=str          - pin state for io command :
                                        'high', 'low', 'disabled', 'input'
    """
    class_arg = [
        {'name': 'frame_id',  'len': 1,      'default': strToByte('0A')},
        {'name': 'address',   'len': 8,
            'default': strToByte('00:00:00:00:00:00:00:00')},
        {'name': 'payload',  'len': None,   'default': None},
        {'name': 'parameter', 'len': 2,      'default': EMPTYBYTE},
        {'name': 'pin',       'len': 1,      'default': None},
        {'name': 'niname',    'len': 20,     'default': b'NewZigbee'},
        {'name': 'pinmode',   'len': 1,      'default': PinMode.DISABLED},
    ]

    def __init__(self, **kwargs):
        self._get_args(**kwargs)
        self.xbee = kwargs['xbee']
        self.destaddrdefault = b'\xFF\xFE'

    def _get_args(self, **kwargs):
        for field in self.class_arg:
            if field['name'] in kwargs:
                if 'pinmode' in field['name']:
                    arg = kwargs[field['name']]
                # ,'pinmode','pin']):
                elif any(s in field['name'] for s in [
                        'pin', 'niname', 'payload']):
                    if type(kwargs[field['name']]) is bytes:
                        arg = kwargs[field['name']]
                    else:
                        arg = bytes(kwargs[field['name']], 'utf-8')
                else:
                    arg = strToByte(kwargs[field['name']])
            else:
                arg = field['default']

            # Do not update value with default if it is already set
            if field['name'] in self.__dict__ and arg == field['default']:
                continue

            self.__dict__[field['name']] = arg

    def _write_conf(self):
        self.xbee.remote_at(
            frame_id=self.frame_id,
            dest_addr=self.destaddrdefault,
            dest_addr_long=self.address,
            command=AtCommand.WRITECHANGES,
            parameter=EMPTYBYTE)

    def self_identify(self):
        self.xbee.at(
            command=AtCommand.NODEIDENTIFY)

#     def self_addr(self):
#         self.xbee.at(
#             command = b'SL')

    def self_pinmode_set(self, **kwargs):
        self._get_args(**kwargs)
        if self.pinmode is None:
            return True
        self.xbee.at(
            frame_id=self.frame_id,
            command=self.pin,
            parameter=self.pinmode,)
        self._write_conf()

    def discovery(self, **kwargs):
        self.xbee.at(
            frame_id=b'0',
            command=AtCommand.DISCOVERY,
            parameter=EMPTYBYTE)

    def ping(self, **kwargs):
        self._get_args(**kwargs)
        self.xbee.remote_at(
            frame_id=self.frame_id,
            dest_addr=self.destaddrdefault,
            dest_addr_long=self.address,
            command=AtCommand.NODEIDENTIFY,
            parameter=EMPTYBYTE)

    def rename(self, **kwargs):
        """
            rename(frame_id=int, address=int16, niname=string)
                Rename Module via a remote AT command 'NI'
        """
        self._get_args(**kwargs)
        self.xbee.remote_at(
            frame_id=self.frame_id,
            dest_addr=self.destaddrdefault,
            dest_addr_long=self.address,
            command=AtCommand.NODEIDENTIFY,
            parameter=self.niname)
        self._write_conf()

    def set_reportpins(self, **kwargs):
        """
            set_pinmode(frame_id=int, address=int16, pin=[, mode=('high',
                        'low', 'input', 'disabled')
            Send IO command for pin state change
        """
        self._get_args(**kwargs)
        if self.parameter is None:
            logger.error('set_reportpins : no parameter !')
            return True
        self.xbee.remote_at(
            frame_id=self.frame_id,
            dest_addr=self.destaddrdefault,
            dest_addr_long=self.address,
            command=AtCommand.PINREPORT,
            parameter=self.parameter,)
        self._write_conf()

    def get_pullmode(self, **kwargs):
        """
            Get PR command for pull up resistors config
        """
        self._get_args(**kwargs)
        self.xbee.remote_at(
            frame_id=self.frame_id,
            dest_addr=self.destaddrdefault,
            dest_addr_long=self.address,
            command=AtCommand.IOPULLMODE,
            parameter=EMPTYBYTE,)

    def set_pullmode(self, **kwargs):
        """
            Send PR command for pull up resistors config
        """
        self._get_args(**kwargs)
        if self.parameter is None:
            logger.error('set_pullmode : no parameter !')
            return True
        self.xbee.remote_at(
            frame_id=self.frame_id,
            dest_addr=self.destaddrdefault,
            dest_addr_long=self.address,
            command=AtCommand.IOPULLMODE,
            parameter=self.parameter,)
        self._write_conf()

    def get_reportpins(self, **kwargs):
        """
            set_pinmode(frame_id=int, address=int16, pin=[, mode=('high',
                        'low', 'input', 'disabled')
            Send IO command for pin state change
        """
        self._get_args(**kwargs)
        self.xbee.remote_at(
            frame_id=self.frame_id,
            dest_addr=self.destaddrdefault,
            dest_addr_long=self.address,
            command=AtCommand.PINREPORT,
            parameter=EMPTYBYTE,)

    def set_pinmode(self, **kwargs):
        """
            set_pinmode(frame_id=int, address=int16, pin=int, mode=('high',
            'low', 'input', 'disabled')
            Send IO command for pin state change
        """
        self._get_args(**kwargs)
        if self.pin is None:
            logger.error('set_pinmode : no pin !')
            return True
        if self.pinmode is None:
            logger.error('set_pinmode : no pinmode !')
            return True
        self.xbee.remote_at(
            frame_id=self.frame_id,
            dest_addr=self.destaddrdefault,
            dest_addr_long=self.address,
            command=self.pin,
            parameter=self.pinmode,)
        self._write_conf()

    def get_pinmode(self, **kwargs):
        """
            get_pinmode(frame_id=int, address=int16, pin=int)
                Request IO pin mode status
        """
        self._get_args(**kwargs)
        self.xbee.remote_at(
            frame_id=self.frame_id,
            dest_addr=self.destaddrdefault,
            dest_addr_long=self.address,
            command=self.pin,
            parameter=EMPTYBYTE,)

    def send(self, **kwargs):
        """
            send(frame_id=int, address=int16, data=string/bytes)
                Send Data command for arduino's controled xbees
        """
        self._get_args(**kwargs)
        if self.payload is None:
            logger.error('xbee.sendHelper.send() : no payload !')
            return True
        self.xbee.tx(
            frame_id=self.frame_id,
            dest_addr=self.destaddrdefault,
            dest_addr_long=self.address,
            data=self.payload)

    def atio(self, **kwargs):
        args = self._get_args(**kwargs)
        self.xbee.at(
            frame_id=self.frame_id,
            command=self.pin,
            parameter=self.pin_mode[args[4]],)


if __name__ == "__main__":
    pass
