import binascii

from core.hosts.xbee import hexdelimiter

import logging
logger = logging.getLogger(__name__)


def byteToStr(arg):
    '''
        Convert byte value readed from an xbee frame to
        a string type that can be used as database entry

    '''
    bArray = bytearray(arg)
    strToReturn = ''
    for item in bArray:
        # add delimiter between hex bytes
        if strToReturn:
            strToReturn += hexdelimiter
        # Convert byte to its hex value
        item = hex(item).replace('0x', '')
        # add leading 0 if < 10
        if (len(item) == 1):
            item = '0' + item
        strToReturn += item
    logger.debug('---> byteToStr(arg) : %s -> %s' % (arg, strToReturn))
    return strToReturn


def strToByte(arg):
    '''
        Convert string value readed from a database entry to
        a byte type that can be used as xbee API parameter

    '''
    byteToReturn = binascii.a2b_hex(arg.replace(hexdelimiter, ''))
    logger.debug('---> strToByte(arg) : %s -> %s' % (arg, byteToReturn))
    return byteToReturn
