from .dataConvert import byteToStr

import logging
logger = logging.getLogger(__name__)

PingRetryId = '0f'


class AtCommand:
    DISCOVERY = b'ND'
    NODEIDENTIFY = b'NI'
    IOPULLMODE = b'PR'
    WRITECHANGES = b'WR'
    PINREPORT = b'IC'


class Status:
    FAILED = b'\x04'
    SUCCESS = b'\x00'


class PinMode:
    DISABLED = b'\x00'
    XBEEMODE = b'\x01'
    INPUT = b'\x03'
    OUTPUTLOW = b'\x04'
    OUTPUTHIGH = b'\x05'


class FrameHelper:
    """
            Class FrameHelper(caller=caller,frame=frame)
            Decode & order xbee frames in string
            format for correct database writing
    """

    def __init__(self, **kwargs):
        self.id = ''
        self.frame_id = ''
        self.status = b''
        self.nodeIdentifier = 'Unknown'
        self.command = b''
        self.address = ''
        self.shortAddress = ''
        self.ioSamples = []
        self.parameter = []
        self.sendedHeader = ''
        self.sendedData = ''
        self.decvalue = None
        self.frame = None
        for name, value in kwargs.items():
            if name == 'frame':
                self.frame = value
                self._decode()
                break

    def _decode(self):
        if self.frame is None:
            logger.error('Trying to decode an empty frame !')
            return True

        for item in self.frame:
            if item == 'id':
                self.id = self.frame['id']
            elif item == 'frame_id':
                self.frame_id = byteToStr(self.frame['frame_id'])
            elif item == 'status':
                self.status = self.frame['status']
            elif item == 'node_identifier':
                try:
                    self.nodeIdentifier = self.parameter[
                        'node_identifier'].decode('utf-8')
                except Exception:
                    self.nodeIdentifier = 'Unknown'
            elif item == 'source_addr_long':
                self.address = byteToStr(self.frame['source_addr_long'])
            elif item == 'source_addr':
                self.shortAddress = byteToStr(self.frame['source_addr'])
            elif item == 'command':
                self.command = self.frame['command']
            elif item == 'samples':
                self.ioSamples = self.frame['samples']
            elif item == 'rf_data':
                self.sendedData = self.frame['rf_data']  # [1:]
            elif item == 'parameter':
                self.parameter = self.frame['parameter']
        if (type(self.parameter) != dict):
            return
        for item in self.parameter:
            if item == 'source_addr_long':
                self.address = byteToStr(self.parameter['source_addr_long'])
            elif item == 'source_addr':
                self.shortAddress = byteToStr(self.parameter['source_addr'])
            elif item == 'node_identifier':
                try:
                    self.nodeIdentifier = self.parameter[
                        'node_identifier'].decode('utf-8')
                except Exception:
                    self.nodeIdentifier = 'Unknown'
        logger.debug('---> decoded result : %s' % self.show())

    def show(self):
        decodeResult = {'id': self.id, 'frame_id': self.frame_id,
                        'status': self.status,
                        'nodeIdentifier': self.nodeIdentifier,
                        'command': self.command,
                        'address': self.address,
                        'shortAddress': self.shortAddress,
                        'ioSamples': self.ioSamples,
                        'parameter': self.parameter,
                        'sendedHeader': self.sendedHeader,
                        'sendedData': self.sendedData}
        return decodeResult
