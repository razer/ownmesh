from django.contrib import admin
from .models import Xbee, XbeeConfig, XbeeIoPin
from core.hosts.admin import BaseHostAdmin, HostPluginConfigAdmin


# @admin.register(XbeeConfig)
# class XbeeConfigAdmin(admin.ModelAdmin):
#     def has_add_permission(self, request):
#         if self.model.objects.count():
#             return False
#         else:
#             super().has_add_permission(request)


@admin.register(XbeeConfig)
class XbeeConfigAdmin(HostPluginConfigAdmin):
    plugin_fields = ('port', 'baudrate')


@admin.register(Xbee)
class XbeeAdmin(BaseHostAdmin):
    plugin_fields = ('address', 'identifier', 'role', 'controled',
                     'ioreportpins', 'iopullmode', 'resetpin', 'powerpin')
    plugin_readonly_fields = ('ioreportpins', 'iopullmode')


admin.site.register(XbeeIoPin)
