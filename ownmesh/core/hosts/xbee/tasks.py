from celery import task
from django.utils import timezone
import re

# Django Models
from .models import Xbee

from core.datas.models import Data
from core.hosts.config import XBEE
from core.hosts.utils import tobytes
from core.hosts.sock import HostSock as HS
# from .utils.sendHelper import SendHelper
from .utils.frameHelper import FrameHelper, Status, AtCommand, PingRetryId
from .utils.dataConvert import strToByte

# Constant values
from core.hosts.xbee.values import OUTPUT
import values.datas as V

import logging
logger = logging.getLogger(__name__)


def _get_instance(address):
    f = Xbee.objects.filter(address=address)
    if not f.count():
        logger.error('Xbee host with address %s not found' % address)
        return
    if f.count() != 1:
        logger.warn('Xbee host : multiple data model with address %s, ' +
                    'taking first' % address)
    return f[0]


def _get_payload(flag):
    payload = HS.SEND + HS.DELIMIT + tobytes(XBEE) + HS.DELIMIT
    payload += b'00:00' + HS.DELIMIT + flag
    return payload


@task(queue='services')
def self_identify_task():
    HS().send(_get_payload(b'SELFIDENTIFY'))


@task(queue='services')
def discovery_task():
    HS().send(_get_payload(b'DISCOVERY'))


@task(queue='services')
def on_new_xbeerx_task(frameId, frame):
    """
        Handle xbee frames received by the dispatcher thread
    """
    def get_iodatamodel(host, header, mode, logtext):
        if not host.data.filter(header=header).count():
            logger.warning('!! {} report for pin {} received from {}, '
                           .format(logtext, header, host.name) +
                           'no data model found : creating it')
            data = Data.objects.create(
                host=host, header=header, entry_type=V.BOOL, mode=mode)
            data.save()
            return data
        return host.data.get(header=header)

    def get_iopinmodel(host, header, logtext):
        mf = host.iopin.filter(pin=header)
        if not mf.count():
            logger.warning('!! {} for pin {} received from {}, '
                           .format(logtext, header, host.name) +
                           'no io pin model found, passing')
            return None
        return mf[0]

    f = FrameHelper(frame=frame)

    ###
    # NODE IDENTIFICATION AT RESPONSE FROM THE COORDINATOR
    ###
    if frameId == 'at_response':
        if not f.address and AtCommand.NODEIDENTIFY in f.command:
            logger.info('Node identification received from coordinator, ' +
                        'daemon is running')
            return False

    # Check the frame for address
    if not f.address:
        logger.warning(
            'AT response frame received with no address, passing : %s' % frame)
        return True

    # Search zigbee in DB
    host = _get_instance(f.address)
    if not host:
        return True

    ###
    # AT RESPONSE frame : Local Xbee responses for at request
    ###
    if frameId == 'at_response':
        logger.debug('*** AT response Frame : %s' % frame)
        if not host:
            if AtCommand.DISCOVERY in f.command:
                # TODO: best solution is to check module config from here
                logger.warning('-> Node discovery response received from a ' +
                               'new zigbee %s with identifier %s, adding it'
                               % (f.address, f.nodeIdentifier))
                # TODO: adding object should not be default but user choice :
                #        set link with db
                #        maybe something like commissioning button stuff
                hostname = f.nodeIdentifier.title()
                host = Xbee.objects.create(name=hostname, shortname=hostname,
                                           added_on=timezone.now(),
                                           address=f.address)
            else:
                logger.warning(
                    'AT response from unknown zigbee with address %s, ' +
                    'passing' % f.address)
            return
        else:
            logger.debug(
                '-> something not yet implemented : %s, passing' % f.command)
            return

    # Exit if no corresponding model in database
    if not host:
        logger.warning('Packet from unknown zigbee with address %s, ' +
                       'passing' % f.address)
        return True
    logger.debug('-> Zigbee with address %s found as %s' %
                 (f.address, host.name))

    ###
    # REMOTE AT RESPONSE frame : Remote Xbee responses for at request
    ###
    if frameId == 'remote_at_response':
        logger.debug('*** REMOTE AT response Frame : %s' % frame)

        # PING Response
        if AtCommand.NODEIDENTIFY in f.command:
            if Status.SUCCESS in f.status:
                logger.info('Node identification received from Xbee {}'
                            .format(host.name))
                # Set Zigbee as online
                host.set_netstatus(True)
                return

#             # Failed status response
#             else:
#                 # Nothing to do if allready set as offline
#                 if not host.is_online():
#                     logger.info('Node identification failure : {} allready '
#                                 .format(host.name) +
#                                 'set as offline, passing')
#                     return
# 
# #                 # Receive second try fail (0x0f in frame id)
# #                 if PingRetryId in f.frame_id:
# #                     logger.info('2d node identification failed for %s, ' +
# #                                 'module is offline' % host.name)
# #                     # Set Zigbee as offline
# #                     host.set_netstatus(False)
# #                     return
# 
#                 # Schedule a ping retry with frame_id 0f
#                 logger.debug('Node identification failed for %s, ' +
#                              'module seems offline' % host.name)
#                 ping_retry_task.apply_async([host], countdown=30)
#                 return

        # IO PULL-UP mode response -> check differences with model & update
        # host if necessary
        if AtCommand.IOPULLMODE in f.command and Status.SUCCESS in f.status:
            # Update on / offline status
            host.set_netstatus(True)
            if not f.parameter:
                logger.error('IO Pullup mode from %s with no value, pass'
                             % host.name)
                return
            logger.info('IO Pullup mode received from %s, value is %s' %
                        (host.name, f.parameter))
            if f.parameter != strToByte(host.iopullmode):
                logger.info('IO Pullup mode from %s does not match, ' +
                            'setting to %s' % (host, host.iopullmode))
                host.get_connector().set_pullmode()
            return

        # IO PINREPORT mode response -> check differences with model & update
        # host if necessary
        if AtCommand.PINREPORT in f.command and Status.SUCCESS in f.status:
            # Update on / offline status
            host.set_netstatus(True)
            if not f.parameter:
                logger.error('IO Pinreport mode from %s : no value, passing'
                             % host.name)
                return
            logger.info('IO Pinreport mode from %s, value is %s'
                        % (host.name, f.parameter))
            if f.parameter != strToByte(host.ioreportpins):
                logger.info('IO Pinreport mode from %s does not match, ' +
                            'setting to %s' % (host.name, host.ioreportpins))
                host.get_connector().set_reportpins()
            return

        # IO state report (-> response to IO command)
        if (re.search(b'd[0-9]', f.command.lower())):
            header = f.command.decode("utf-8").lower()

            # Get related models : iopin from xbee + data
            iopin = get_iopinmodel(host, header, 'IO report')
            if not iopin:
                return

            # Get or create data model
            data = get_iodatamodel(host, header, V.WRITEONLY, 'IO report')

            if f.status == Status.FAILED:
                logger.warning('%s : Failed status for pinmode request - ' +
                               'pin %s : data is pending'
                               % (host.name, f.command))
                ping_retry_task.s(host).apply_async(countdown=30)
                if not data.pending:
                    data.pending = True
                    data.save()
                return

            # Update on / offline status
            host.set_netstatus(True)

            if f.parameter:
                logger.debug('%s : Pinmode status request - %s have pinmode %s'
                             % (host.name, f.command, f.parameter))
                return

            # Frame with Status.SUCCESS and no parameter -> response to pinmode
            # command
            logger.info('Xbee %s : Pinmode remote at response success for %s'
                        % (host.name, f.command))
            if data.pending:
                data.pending = False
                data.save()

        elif (Status.FAILED in f.status):
            logger.warning(
                'Failed status from remote at command %s' % f.command)

        else:
            logger.debug(
                'Remote AT for something not implemented : %s' % f.command)

    ###
    # IO SAMPLES frame : Xbee send it's Io state when changes are detected
    ###
    if frameId == 'io_data':
        logger.debug('*** IO Samples Frame : %s' % frame)
        # Update on / offline status
        host.set_netstatus(True)
        # Look at all Io in frame content
        for pin in (f.ioSamples[0].keys()):
            header = pin.replace('io-', '')  # .upper()
            # Get related iopin model
            iopin = get_iopinmodel(host, header, 'IO state change')
            if not iopin:
                continue

            if iopin.mode == OUTPUT:
                logger.debug('IO change for %s from %s, pin is output, passing'
                             % (header, host.name))
                continue

            data = get_iodatamodel(host, header, V.READONLY, 'IO state change')

            # Get value from frame
            value = bool(f.ioSamples[0][pin])

            # Invert value if inverted logic enabled
            if iopin.inverted_logic:
                value = not value

            # Do not create entry if no change in value
            if data.latest_value == value:
                logger.debug('IO %s (%s) from %s with value %s : no change'
                             % (data.name, data.header, host.name, value))
                continue

            logger.debug('IO %s for %s from %s with value %s , new entry'
                         % (data.name, data.header, host.name, value))
            # Set io state value in DB
            data.set_value(value, direction=V.RECEIVED)

    ###
    # DATA frame
    ###
    elif frameId == 'rf_data':
        logger.debug('*** DATA Frame : %s' % frame)
        logger.info('*** DATA received : %s' % f.sendedData)
        # Case zigbee badly configured in db, correct it
        if not host.controled:
            logger.warning('Data from %s : set controled flag to true'
                           % host.name)
            host.controled = True
            host.save()
        host.get_payload(f.sendedData)


@task(queue='services')
def ping_retry_task(host):
    pass
#     host.get_connector().ping(frame_id=PingRetryId)
