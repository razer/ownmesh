from django.db import models
import re
from core.hosts.models import Host, HostPluginConfig
from core.hosts.utils import tobytes
# from core.hosts.xbee import ZigBee, Serial, Dispatch
from .utils.frameHelper import PinMode
from .utils.dataConvert import strToByte
# from core.hosts.xbee.connector import Connector
from django.core.validators import MaxValueValidator, MinValueValidator
from values.hosts import FRAME as F
import core.hosts.xbee.values as V

# Logger
import logging
logger = logging.getLogger(__name__)


class XbeeConfig(HostPluginConfig):
    port = models.TextField(default='/dev/ttyS1', max_length=16)
    baudrate = models.IntegerField(default=9600)


class Xbee(Host):
    address = models.TextField(
        default='00:00:00:00:00:00:00:00', max_length=23)
    shortadr = models.TextField(default='FF:FE', max_length=5)
    identifier = models.CharField(default='noid', max_length=10)
    # Coordinator 'CR' / Router 'RO' / End Node 'EN'
    role = models.CharField(max_length=2,
                            choices=V.TYPE_CHOICES,
                            default=V.ENDNODE)
    controled = models.BooleanField(default=False)
    # Pins enabled for IO change detection (IC AT command) as a mask
    ioreportpins = models.TextField(default='00:00', max_length=5)
    # Pins pull-up/down mode (0 means pull-down)
    iopullmode = models.TextField(default='1F:FF', max_length=5)
    # Pin used for controler (arduino) reset
    resetpin = models.IntegerField(
        default=-1, validators=[MaxValueValidator(8), MinValueValidator(-1)])
    # Pin used for controler power supply (sleep mode)
    powerpin = models.IntegerField(
        default=-1, validators=[MaxValueValidator(8), MinValueValidator(-1)])

    def __str__(self):
        return self.name

    def get_sendheader(self):
        return self.address

    def _get_formated_hexvalue(self, value):
        if value < 16:
            return '00:0%s' % hex(value).replace('0x', '')
        elif value < 256:
            return '00:%s' % hex(value).replace('0x', '')
        elif value < 4096:
            return '0%s:%s' % (hex(value).replace('0x', '')[0],
                               hex(value).replace('0x', '')[1:])
        else:
            return '%s:%s' % (hex(value).replace('0x', '')[0:2],
                              hex(value).replace('0x', '')[2:])

    def set_ioreportpins(self):
        '''
            Set io report pins parameter for io change detection,
            according to IoPin models found
        '''
        enabledPins = []
        for iopin in self.xbee.iopin.all():
            enabledPins.append(int(iopin.pin.lower().replace('d', '')))

        reportValue = 0
        for pin in range(0, 9):
            for enabledPin in enabledPins:
                if enabledPin is pin:
                    reportValue += 2**pin
        if self.ioreportpins != self._get_formated_hexvalue(reportValue):
            self.ioreportpins = self._get_formated_hexvalue(reportValue)
            self.save()
        return self.ioreportpins

    def set_iopullmode(self):
        pdownPins = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0]
        for iopin in self.xbee.iopin.all():
            pin = int(iopin.pin.lower().replace('d', ''))
            if iopin.mode == V.INPUTDOWN:
                if pin < 5:
                    pdownPins[4 - pin] = 0
                elif pin == 6:
                    pdownPins[5] = 0
                elif pin == 8:
                    pdownPins[6] = 0
                elif pin == 9:
                    pdownPins[9] = 0
            # Special handling for pin 7 - respect default xbee conf
            elif iopin.mode == V.INPUTUP:
                if pin == 7:
                    pdownPins[13] = 1

        pdownValue = 0
        for i in range(0, 14):
            pdownValue += pdownPins[i] * 2**i

        if self.iopullmode != self._get_formated_hexvalue(pdownValue):
            self.iopullmode = self._get_formated_hexvalue(pdownValue)
            self.save()
        return self.iopullmode

    def send(self, payload=b'', ping=False, check=False, overwritten=False):
        payload = tobytes(payload)
        pl_item = payload.split(F.DELIMIT)
        if ping and not self.controled:
            logger.info('Xbee specific ping')
            super(Xbee, self).send(payload=b'NODEIDENTIFY', overwritten=True)
            return
        if (len(pl_item) > 1 and F.TXDT in payload[2:3]):
            for si in payload.split(F.DELIMIT)[1:]:
                if re.search('d[0-9]', si[:2].lower().decode('utf-8')):
                    pin = si[:2].upper()
                    # IO pinmode & output pins state
                    if len(si) == 3:
                        # output state
                        pinmode = PinMode.OUTPUTLOW
                        if b'1' in si[2:3]:
                            pinmode = PinMode.OUTPUTHIGH
                        logger.info(f'Set output pin {si[:2].decode("utf-8")} '
                                    + f'to mode {pinmode}')
                    else:
                        # pinmode
                        logger.info(f'Set pin {si[:2].decode("utf-8")} '
                                    + f'to mode {si[2:2]}')
                        pinmode = strToByte(si[2:2].decode('utf-8'))

                    pl = b'PINMODE' + F.DELIMIT + pin + F.DELIMIT + pinmode
                    super(Xbee, self).send(payload=pl, ping=ping,
                                           check=check, overwritten=True)
                    payload = payload.replace(si, b'')

        super(Xbee, self).send(payload=payload, ping=ping, check=check,
                               overwritten=True)

    def save(self, *args, **kwargs):
        super(Xbee, self).save(*args, **kwargs)
        h = Host.objects.get(id=self.id)
        h.save()


class XbeeIoPin(models.Model):
    xbee = models.ForeignKey(Xbee, related_name='iopin',
                             on_delete=models.CASCADE)
    # D0, D1...
    pin = models.CharField(max_length=2,
                           choices=V.IOPIN_CHOICES,
                           default='d0')
    # input pull-up 'IU' or input pull-down 'ID' or output 'OU'
    mode = models.CharField(max_length=2,
                            choices=V.IOMODE_CHOICES,
                            default=V.OUTPUT)
    inverted_logic = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super(XbeeIoPin, self).save(*args, **kwargs)
        # Update model
        self.xbee.set_ioreportpins()
        self.xbee.set_iopullmode()
        # Update host conf
        payload = b'PINMODE' + F.DELIMIT + tobytes(self.pin) + F.DELIMIT
        if self.mode != V.OUTPUT:
            payload += tobytes(PinMode.INPUT)
        elif self.inverted_logic:
            payload += tobytes(PinMode.OUTPUTHIGH)
        else:
            payload += tobytes(PinMode.OUTPUTLOW)
        host = Host.objects.get(pk=self.xbee.pk)
        host.send(payload=payload, overwritten=True)
        payload = b'REPORTPINS' + F.DELIMIT
        payload += tobytes(self.xbee.ioreportpins)
        host.send(payload=payload, overwritten=True)
        payload = b'PULLMODE' + F.DELIMIT
        payload += tobytes(self.xbee.iopullmode)
        host.send(payload=payload, overwritten=True)

    def delete(self, *args, **kwargs):
        logger.info('Delete pin')
        super(XbeeIoPin, self).delete(*args, **kwargs)
        # Update model
        self.xbee.set_ioreportpins()
        self.xbee.set_iopullmode()
        # Update host conf
        host = Host.objects.get(pk=self.xbee.pk)
        payload = b'REPORTPINS' + F.DELIMIT
        payload += tobytes(self.xbee.ioreportpins)
        host.send(payload=payload, overwritten=True)
        payload = b'PULLMODE' + F.DELIMIT
        payload += tobytes(self.xbee.iopullmode)
        host.send(payload=payload, overwritten=True)

#         self.xbee.get_connector().set_reportpins()
#         self.xbee.get_connector().set_pullmode()
#         self.xbee.get_connector().set_pinmode(self.pin, PinMode.DISABLED)

    def __str__(self):
        return f'{self.pin} ({self.xbee.name})'
