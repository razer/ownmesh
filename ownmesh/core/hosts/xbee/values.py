COORDINATOR = 'CR'
ROUTER = 'RO'
ENDNODE = 'EN'
TYPE_CHOICES = (
    (COORDINATOR,   'Coordinator'),
    (ROUTER,        'Router'),
    (ENDNODE,       'End node'),
)

IOPIN_CHOICES = (
    ('d0', 'AD0/DIO0 (20)'),
    ('d1', 'AD1/DIO1 (19)'),
    ('d2', 'AD2/DIO2 (18)'),
    ('d3', 'AD3/DIO3 (17)'),
    ('d4', 'AD4/DIO4 (11)'),
    ('d6', 'RTS/AD6/DIO6 (16)'),
    ('d7', 'CTS/DIO7 (12)'),
    ('d8', 'DTR/DI8 (9) / DO8 (4)'),
)

INPUTUP = 'IU'
INPUTDOWN = 'ID'
OUTPUT = 'OU'
IOMODE_CHOICES = (
    (INPUTUP,       'State report from an input (pull-up)'),
    (INPUTDOWN,     'State report from an input (pull-down)'),
    (OUTPUT,        'Remote output IO control'),
)
