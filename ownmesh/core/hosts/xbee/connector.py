# from celery import task
# import re
import sys
import asyncio
from .tasks import on_new_xbeerx_task
from .utils.sendHelper import SendHelper
# from .utils.frameHelper import PinMode
# from .utils.dataConvert import strToByte
from values.hosts import FRAME as F
# from core.hosts.xbee.config import *  # @UnusedWildImport
from core.hosts.pluginConnector import PluginConnector
from . import ZigBee, Serial, Dispatch  # , values as V

# Logger
import logging
from core.hosts.utils import tobytes
from datetime import datetime, timedelta
logger = logging.getLogger(__name__)


class Connector(PluginConnector):

    def __init__(self, *args, **kwargs):
        super(Connector, self).__init__(*args, **kwargs)
        self.ser = None
        self.xbee = None
        self.frameContent = None
        self.last_atcommand = datetime.now()
        self.atcommand_delay = timedelta(seconds=1)

    def begin(self):
        super(Connector, self).begin()
        try:
            self.ser = Serial(self.config.port, self.config.baudrate)
            self.xbee = ZigBee(self.ser)
            self.dispatch = Dispatch(xbee=self.xbee)
            self.sendhelper = SendHelper(xbee=self.xbee)
        except Exception:
            logger.error(self.error)
            logger.error(sys.exc_info()[1])
            return False

        self.dispatched = False
        self.dispatch.register(
            'io_data',
            self.dispatcher,
            lambda frame: (
                frame['id'] == 'rx_io_data'
                or frame['id'] == 'rx_io_data_long_addr')
        )

        self.dispatch.register(
            'at_response',
            self.dispatcher,
            lambda frame: frame['id'] == 'at_response'
        )

        self.dispatch.register(
            'remote_at_response',
            self.dispatcher,
            lambda frame: frame['id'] == 'remote_at_response'
        )

        self.dispatch.register(
            'rf_data',
            self.dispatcher,
            lambda frame: self._frame_is_data(frame)
        )

        self.dispatch.register(
            'unsupported',
            self.unsupported,
            lambda frame: True
        )
        self.xbee = ZigBee(self.ser, callback=self.dispatch.dispatch)
        logger.info('Xbee connector ready')
        return True

    def _frame_is_data(self, frame):
        if 'rf_data' in frame.keys():
            return True
        else:
            return False

    def unsupported(self, name, packet):
        if not self.dispatched:
            logger.info(f'*** Packet {packet} is not yet implemented')
        else:
            self.dispatched = False

    def dispatcher(self, frameId, frame):
        # Inform that packet has been dispatched
        self.dispatched = True
        on_new_xbeerx_task.delay(frameId, frame)  # @UndefinedVariable

    def stop(self):
        '''
            Stop connector
        '''
        self.xbee.halt()

    async def _write(self, address, payload):
        '''
            Send to host bytes payload
        '''
#         logger.info('datetime.now() : {}'.format(datetime.now()))
#         logger.info('self.last_atcommand : {}'.format(self.last_atcommand))
#         logger.info('Result : {}'.format(datetime.now()-self.last_atcommand))
        while (datetime.now() - self.last_atcommand < self.atcommand_delay):
            await asyncio.sleep(.1)
        payload = tobytes(payload)
        logger.debug('Payload : %s' % payload)
        pl_items = payload.split(F.DELIMIT)
        if b'PINMODE' in pl_items[0]:
            logger.info('Xbee %s : pinmode %s to %s'
                        % (address, pl_items[1], pl_items[2]))
            self.last_atcommand = datetime.now()
            self.sendhelper.set_pinmode(address=address, pin=pl_items[1],
                                        pinmode=pl_items[2])
            return
        if b'REPORTPINS' in pl_items[0]:
            if len(pl_items) < 2:
                logger.info('Xbee %s : get report pins' % address)
                self.last_atcommand = datetime.now()
                self.sendhelper.get_reportpins(address=address)
                return
            logger.info('Xbee %s : set report pins to %s'
                        % (address, pl_items[1]))
            self.last_atcommand = datetime.now()
            self.sendhelper.set_reportpins(
                address=address, parameter=pl_items[1].decode('utf-8'))
            return
        if b'PULLMODE' in pl_items[0]:
            if len(pl_items) < 2:
                logger.info('Xbee %s : get pullmode pins' % address)
                self.last_atcommand = datetime.now()
                self.sendhelper.get_pullmode(address=address)
                return
            logger.info('Xbee %s : set pullmode pins to %s'
                        % (address, pl_items[1]))
            self.last_atcommand = datetime.now()
            self.sendhelper.set_pullmode(
                address=address, parameter=pl_items[1].decode('utf-8'))
            return
        if b'SELFIDENTIFY' in pl_items[0]:
            logger.info('Xbee self identify')
            self.sendhelper.self_identify()
            return
        if b'DISCOVERY' in pl_items[0]:
            logger.info('Xbee discovery')
            self.sendhelper.discovery()
            return
        if b'NODEIDENTIFY' in payload:
            logger.info('Xbee node identify %s' % address)
            self.last_atcommand = datetime.now()
            self.sendhelper.ping(address=address)
            return

        logger.info('Xbee send data %s to %s' % (payload, address))
        self.sendhelper.send(address=address, payload=payload)

    def write(self, transport, payload):
        '''
            Send to host bytes payload
        '''
        self.loop.create_task(self._write(transport, payload))
