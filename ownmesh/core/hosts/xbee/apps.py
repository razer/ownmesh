from django.apps import AppConfig


class XbeeConfig(AppConfig):
    name = 'core.hosts.xbee'
