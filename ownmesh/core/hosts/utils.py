import importlib


def tobytes(stream):
    if type(stream) is bytes:
        return stream
    if type(stream) is not str:
        stream = str(stream)
    return bytes(stream, 'utf-8')


def get_host_config(hosttype_str):
    config_lib = importlib.import_module(
        f'core.hosts.{hosttype_str}.models')
    config_model = getattr(config_lib, f'{hosttype_str.capitalize()}Config')
    return config_model.objects.get_or_create()[0]
