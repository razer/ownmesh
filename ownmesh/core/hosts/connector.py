import os
import sys
import asyncio
import importlib
import logging
from threading import Thread
from contextlib import suppress
from django.utils.translation import ugettext_lazy as _

from project.settings import HOST_SOCK
from core.models import Config
from values.hosts import (FRAME as F, STOPPED, RUNNING, FAILED)
from .models import Host
from .sock import HostSock as HS
from .config import LOCAL, HOSTTYPES
from .utils import get_host_config

logger = logging.getLogger(__name__)


class Connector(Thread):

    def __init__(self, loop=None):
        super(Connector, self).__init__()
        self.daemon = True
        self.config = Config.objects.get_or_create()[0]
        self.loop = loop
        self.connector = {}
        self.sock_server = None
        with suppress(OSError):
            os.remove(HOST_SOCK)

    def _get_host(self, pk):
        host_filter = Host.objects.filter(pk=int(pk))
        if not host_filter.count():
            logger.error(f'No host with pk {pk}')
            return None
        return host_filter[0].get_model()

    def _get_connector(self, host_type):
        if host_type not in self.connector.keys():
            logger.info(f'{host_type} not loaded')
            return None
        return self.connector[host_type]

    def _strip_payload(self, payload, min_len=1):
        payload = payload.strip().decode('utf-8')
        payload = payload.split(HS.STR_DELIMIT)
        payload.pop(0)
        if len(payload) < min_len:
            logger.error(f'Payload too short : {payload}')
            return False
        return payload

    async def sock_handler(self, reader, writer):  # pylint: disable=R0911
        payload = await reader.readline()

        if HS.STATUS in payload:
            logger.debug(f'Sending status response {payload}')
            writer.write(HS.RUNNING + HS.ENDLINE)
            await writer.drain()
            writer.close()
            return

        if HS.PING + HS.DELIMIT in payload:
            payload = self._strip_payload(payload)
            if not payload:
                return
            connector = self._get_connector(payload[0])
            if not connector:
                return
            connector.write(payload[1], F.STARTSEQ + F.PING)
            return

        if HS.SEND + HS.DELIMIT in payload:
            payload = self._strip_payload(payload, min_len=3)
            if not payload:
                return
            connector = self._get_connector(payload[0])
            if not connector:
                return
            logger.debug(f'{payload[0]}: sending {payload[2]} to {payload[1]}')
            connector.write(payload[1], payload[2])
            return

        if HS.RELOAD + HS.DELIMIT in payload:
            payload = self._strip_payload(payload)
            if not payload:
                return
            logger.info(f'Reload request {payload}')
            if HS.GLOBAL in payload[0]:
                for connector in self.connector.values():
                    connector.stop()
                # Ensure database update
                await asyncio.sleep(1)
                self.config.refresh_from_db()
                self.get_connectors()
                logger.info('Host connectors event loop restarted')
                return
            hosttype = payload[0]
            connector = self._get_connector(hosttype)
            host_config = get_host_config(hosttype)
            if not connector:
                self._start_connector(hosttype, host_config)
            else:
                self.loop.create_task(connector.reload())

        if HS.STOP + HS.DELIMIT in payload:
            payload = self._strip_payload(payload)
            if not payload:
                return
            logger.info(f'Plugin stop request {payload}')
            hosttype = payload[0]
            connector = self._get_connector(hosttype)
            host_config = get_host_config(hosttype)
            if not connector:
                return
            connector.stop()
            self.connector.pop(hosttype, 0)
            if host_config.status.state not in STOPPED:
                host_config.status.state = STOPPED
                host_config.status.save()

    async def test(self):
        while True:
            logger.debug('Host listener event loop test')
            await asyncio.sleep(30)

    def sock_handler_cb(self, reader, writer):
        self.loop.create_task(self.sock_handler(reader, writer))

    def signal_handler(self, signal, frame):
        self.loop.stop()
        self.sock_server.close()
        os.remove(HOST_SOCK)
        sys.exit(0)

    def _start_connector(self, hosttype, host_config):
        host_config.status.error = ''
        try:
            connector_lib = importlib.import_module(
                'core.hosts.%s.connector' % hosttype)
            self.connector[hosttype] = connector_lib.Connector(
                loop=self.loop, config=host_config)
            if self.connector[hosttype].begin():
                host_config.status.state = RUNNING
                host_config.status.save()
                return True
        except Exception:
            err = sys.exc_info()[:3]
            logger.error(f'Plugin {hosttype} raised {err}')
            host_config.status.error = _(
                f'Raised exception : {err}')
        if not host_config.status.error:
            host_config.status.error = self.connector[hosttype].error
        host_config.status.state = FAILED
        host_config.status.save()
        return False

    def get_connectors(self):
        self.connector = {}
        for hosttype in HOSTTYPES:
            if LOCAL in hosttype:
                continue
            host_config = get_host_config(hosttype)
            if host_config.status.state not in STOPPED:
                host_config.status.state = STOPPED
                host_config.status.save()
            if not host_config.enabled:
                continue
            self._start_connector(hosttype, host_config)

    def run(self):
        asyncio.set_event_loop(self.loop)
        self.sock_server = asyncio.start_unix_server(
            self.sock_handler_cb, path=HOST_SOCK, loop=self.loop)
        # os.chmod(HOST_SOCK, 0o666)
        self.loop.run_until_complete(self.sock_server)
        self.loop.create_task(self.test())
        self.get_connectors()
        try:
            os.chmod(HOST_SOCK, 0o775)
        except Exception:
            logger.error(sys.exc_info()[:3])
        logger.info('Host event loop ready')
        self.loop.run_forever()
