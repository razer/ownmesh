# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-03-20 16:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0003_hostpluginstatus'),
    ]

    operations = [
        migrations.AddField(
            model_name='host',
            name='last_tx_pending',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='host',
            name='power_mode',
            field=models.CharField(choices=[('NO', 'Always on'), ('SL', 'Wake up on request'), ('DS', 'Deep sleep : no listening')], default='NO', max_length=2),
        ),
    ]
