import asyncio
import logging
from django.utils.translation import ugettext_lazy as _

logger = logging.getLogger(__name__)


class PluginConnector:

    def __init__(self, loop=None, config=None):
        self.hosttype = config.hosttype
        self.config = config
        self.loop = loop
        self.read_task = None
        self.read_delay = 0.2
        self.error = _(f'{self.hosttype} plugin fail to start. ' +
                       'Check your config and hardware')

    def begin(self):
        '''
            Initialize plugin
        '''
        pass

    async def write(self, address, payload):
        '''
            Send to host bytes payload
        '''
        await asyncio.sleep(2)

    def stop(self):
        '''
            Stop connector
        '''
        if self.read_task:
            logger.info('Cancelling task')
            self.read_task.cancel()

    async def reload(self):
        '''
            Reload connector (model change signal)
        '''
        self.stop()
        # Ensure database update
        await asyncio.sleep(1)
        self.config.refresh_from_db()
        logger.info(f'Reloading {self.hosttype} connector')
        self.begin()

    async def read(self):
        '''
            Read/Listen for data
        '''
        pass
