import logging
from django.apps import AppConfig

logger = logging.getLogger(__name__)


class HostPluginConfig(AppConfig):
    name = 'core.hosts'

    def ready(self):
        logger.debug('Host app signals init')
        # flake8: noqa
        from . import signals as hostsignals  # pylint: disable=W0612
