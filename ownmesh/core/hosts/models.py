import logging
from time import strftime
from datetime import timedelta
from django.db import models, DatabaseError
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

import values.hosts as V
from values.hosts import FRAME as F, RequestHeader as RH
import values.datas as VD
from values.actions import DAYOFWEEK
from .utils import tobytes
from .config import (HOSTTYPES, LOCAL)  # READPERIOD_CHOICES
from .sock import HostSock as HS

logger = logging.getLogger(__name__)


class HostPluginStatus(models.Model):
    error = models.TextField(max_length=50, default='', blank=True)
    state = models.TextField(choices=V.PLUGIN_STATE, default=V.STOPPED)


class HostPluginConfig(models.Model):

    class Meta:
        abstract = True
    enabled = models.BooleanField(default=False)
    status = models.OneToOneField(HostPluginStatus, blank=True, null=True,
                                  on_delete=models.CASCADE)

    @property
    def running(self):
        if not self.status:
            return False
        if V.RUNNING in self.status.state:
            return True
        return False

    @property
    def hosttype(self):
        return self.__class__.__name__.replace('Config', '').lower()

    def __str__(self):
        return str(_(f'Configuration for {self.hosttype} plugin'))

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.status:
            self.status = HostPluginStatus.objects.create()

        if self.enabled:
            payload = (HS.RELOAD + HS.DELIMIT + tobytes(self.hosttype) +
                       HS.ENDLINE)
            logger.debug(f'Plugin {self.hosttype} : sending reload request')
            HS().send(payload)
        else:
            if V.RUNNING in self.status.state:
                payload = (HS.STOP + HS.DELIMIT + tobytes(self.hosttype) +
                           HS.ENDLINE)
                logger.debug(f'Plugin {self.hosttype} : sending stop request')
                HS().send(payload)

        return models.Model.save(self, force_insert=force_insert,
                                 force_update=force_update,
                                 using=using,
                                 update_fields=update_fields)


class Host(models.Model):
    name = models.TextField(default='Unknown', max_length=30)
    enabled = models.BooleanField(default=True)
    added_on = models.DateTimeField(default=timezone.now, blank=True)
    edited_on = models.DateTimeField(default=timezone.now, blank=True)
    guicolor = models.CharField(
        max_length=8, choices=V.GUICOLOR_CHOICES, default='gry')
    power_mode = models.CharField(
        max_length=2, choices=V.POWERMODE_CHOICES, default=V.ALWAYS_ON)
    last_tx_pending = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.edited_on = timezone.now()
        super(Host, self).save(force_insert=force_insert,
                               force_update=force_update,
                               using=using, update_fields=update_fields)

    def _get_host_attributes(self):
        attributes = {'type': None, 'model': None}
        for validtype in HOSTTYPES:
            if hasattr(self, validtype):
                attributes['type'] = validtype
                attributes['model'] = getattr(self, validtype)
                break
        return attributes

    def get_model(self):
        return self._get_host_attributes()['model']

    def get_type(self):
        return self._get_host_attributes()['type']

    @property
    def online(self):
        if self.get_type() == LOCAL:
            return True
        if self.power_mode in V.DEEP_SLEEP:
            return False
        try:
            # pylint: disable=E1101
            online = self.netstatus_data.latest_value
        except Exception:
            return False
        if online is None:
            return False
        return online

    def check_netstatus(self):
        if self.set_netstatus(None):
            return True
        # pylint: disable=E1101
        self.netstatus_data.wait_response = True
        self.netstatus_data.save()
        self.send(ping=True)
        return False

    def set_netstatus(self, online):
        if self.get_type() == LOCAL or self.power_mode in V.DEEP_SLEEP:
            return None
        if not hasattr(self, 'netstatus_data'):
            # Force saving instance for netStatus data creation
            self.save()
            return None
        # pylint: disable=E1101
        if self.netstatus_data.wait_response:
            self.netstatus_data.wait_response = False
            self.netstatus_data.save()

        if online is None:
            return False
        if self.netstatus_data.latest_value == online:
            logger.debug(f'Netstatus for {self.name} : no change ({online})')
            return False
        self.netstatus_data.set_value(online, direction=VD.RECEIVED)
        return True

    def get_values(self, mode=VD.READONLY):
        stream = F.STARTSEQ + F.TXRQ
        count = 0
        for data in self.data.filter(mode=mode):
            # Limit amount of datas per frame (low mem receivers)
            if count > 10:
                self.send(stream)
                stream = F.STARTSEQ + F.TXRQ
                count = 0
            stream += F.DELIMIT
            if len(data.header) > 1:
                stream += tobytes(data.header)
            else:
                stream += b'0' + tobytes(data.header)
            count += 1
        self.send(stream, check=True)

    def set_clock(self):
        self.send(F.STARTSEQ + F.TXVL + F.DELIMIT + RH.CLOCK +
                  tobytes(strftime('%H%M%d%m')) +
                  DAYOFWEEK[int(strftime('%w'))])

    def set_values(self, clock=False, onlypending=False):
        if clock:
            self.set_clock()
        data_map = self.data.exclude(mode=VD.READONLY)
        data_map = data_map.exclude(pending=False) if onlypending else data_map
        if not data_map.count():
            return
        payload = F.STARTSEQ + F.TXDT
        count = 0
        for data in data_map:
            data_payload = data.serialize()
            if not data_payload:
                continue
            # Limit amount of datas per frame (low mem receivers)
            if count > 10:
                self.send(payload)
                payload = F.STARTSEQ + F.TXDT
                count = 0
            payload += data_payload
            count += 1
        if count:
            self.send(payload, check=True)

    def send(self, payload=b'', ping=False, check=False, overwritten=False):
        # Ensure that if send() is overrided by plugin, it is used
        if not overwritten:
            real_host = self.get_model()
            real_host.send(payload=payload, ping=ping, check=check,
                           overwritten=True)
            return
        payload = tobytes(payload)
        if check:
            if len(payload) <= len(F.STARTSEQ + F.TXDT):
                logger.warning(
                    f'{payload} to {self.name} : frame too short !!!')
                return
        if ping:
            header = HS.PING + HS.DELIMIT
        else:
            header = HS.SEND + HS.DELIMIT
        header += tobytes(self.get_type()) + HS.DELIMIT
        header += tobytes(self.get_model().get_sendheader())
        logger.debug(f'Sending {header + HS.DELIMIT + payload} to {self}')
        HS().send(header + HS.DELIMIT + payload + HS.DELIMIT)

    def get_data(self, header):
        if isinstance(header, bytes):
            header = header.decode('utf-8')
        f = self.data.filter(header=header)
        if not f.count():
            logger.error(f'{self.name}: no data model with header {header}')
            return None
        if f.count() != 1:
            logger.warning(
                f'{self.name}: multiple data model with header {header} !!!')
        return f[0]

    def _set_pending_values(self):
        # Avoid multiple set_values requests during ping flooding
        if timezone.now() - self.last_tx_pending > timedelta(seconds=10):
            self.last_tx_pending = timezone.now()
            self.save()
            self.set_values(onlypending=True)

    # pylint: disable=R0911
    def get_payload(self, payload):
        self.set_netstatus(True)
        payload = tobytes(payload)

        if payload[:len(F.DEBUGSEQ)] == F.DEBUGSEQ:
            logger.info(
                f'Debug msg from {self.name} : {payload[len(F.DEBUGSEQ):]}')
            return False

        if payload[:len(F.STARTSEQ)] != F.STARTSEQ:
            logger.error(f'{payload} from {self.name} : bad start sequence')
            return True

        payload = payload[len(F.STARTSEQ):].strip()

        if F.RXER in payload[0:1]:
            logger.warning(
                f'RXER (error resp to data tx) : {payload} from {self.name}')
            return True

        if F.PING in payload[0:1]:
            self.send(F.STARTSEQ + F.PONG)
            self._set_pending_values()
            return False

        if F.PONG in payload[0:1]:
            self._set_pending_values()
            return False

        item = payload.split(F.DELIMIT)
        if len(item) <= 1:
            logger.warning(f'{payload} from {self.name} : No item found')
            return True

        if F.RXOK in payload[0:1]:
            logger.info(f'RXOK : {payload}')
            item.pop(0)
            for si in item:
                if si in F.DELIMIT:
                    continue
                if len(si) != 2:
                    logger.error(f'RXOK : bad item size {si}')
                    continue
                header = si[0:]

                data = self.get_data(header)
                if not data:
                    continue

                # TODO: check if tx data and local have same value
                if data.pending:
                    logger.info(f'{data.shortname} -> pending to false')
                    data.pending = False
                    data.save()

        elif F.TXDT in payload[0:1]:
            logger.info(f'TXDT : {payload}')
            payload = F.STARTSEQ + F.RXOK
            item.pop(0)
            count = 0
            for si in item:
                if si in F.DELIMIT:
                    continue
                if len(si) <= 2:
                    logger.error(f'TXDT : item too short {si}')
                    continue
                header = si[:2]
                value = si[2:]
                logger.debug(f'TXDT item : header {header}, value {value}')

                # Search data in DB
                data = self.get_data(header)
                if not data:
                    continue
                if data.mode == VD.WRITEONLY:
                    logger.error(f'TXDT from {self.name} : write only flag ' +
                                 f'on {data.shortname} !!!')
                    continue
                # Update database with new value
                try:
                    data.set_value(value, direction=VD.RECEIVED)
                except (ValueError, DatabaseError):
                    logger.error(f'TXDT from {self.name} : db write failed ' +
                                 f'for {data.shortname} !!!')
                    continue
                # Limit payload size to 8 items (mem issue with arduino)
                if count > 8:
                    self.send(payload)
                    payload = F.STARTSEQ + F.RXOK
                    count = 0
                payload += F.DELIMIT + header
                count += 1

            if len(payload) > len(F.STARTSEQ + F.RXOK):
                self.send(payload)

        elif F.TXRQ in payload[0:1]:
            logger.info(f'TXRQ (data request) : {payload}')
            payload = None
            item.pop(0)
            for si in item:
                if len(si) != 2:
                    logger.error(f'TXRQ : bad item {si}')
                    continue
                header = si[0:]

                if RH.CLOCK in header:
                    logger.info(f'TXRQ : Clock request from {self.name}')
                    self.set_clock()
                    continue
                if RH.ALLVALUES in header:
                    logger.info(f'TXRQ : All data request from {self.name}')
                    # self.set_clock()
                    self.set_values()
                    continue

                # search data in DB
                data = self.get_data(header)
                if not data:
                    logger.error(f'TXRQ : header {header} from {self.name}, ' +
                                 f'no data found')
                    continue

                data.pending = True
                data.save()
                payload = data.serialize()

            if payload:
                self.send(F.STARTSEQ + F.TXDT + payload)
        return None
