import socket
import sys
import logging
from project.settings import HOST_SOCK

logger = logging.getLogger(__name__)


class UnixSock:
    DELIMIT = b':'
    ENDLINE = b'\n'
    STATUS = b'STATUS'
    RUNNING = b'RUNNING'

    def __init__(self, sockfile=None):
        self.sockfile = sockfile
        self.sock = socket.socket(socket.AF_UNIX)
        self.sock.settimeout(3.0)

    def send(self, payload, response=False):
        recv = None
        try:
            self.sock.connect(self.sockfile)
        except Exception:
            logger.error(sys.exc_info()[1])
            return
        if UnixSock.ENDLINE not in payload:
            payload += UnixSock.ENDLINE
        self.sock.send(payload)
        if response:
            recv = self.sock.recv(64)
        self.sock.close()
        return recv


class HostSock(UnixSock):
    DELIMIT = b'!@!'
    STR_DELIMIT = DELIMIT.decode('utf-8')  # @UndefinedVariable
    PING = b'PING'
    SEND = b'SEND'
    RELOAD = b'RELOAD'
    STOP = b'STOP'
    GLOBAL = 'global'

    def __init__(self):
        super(HostSock, self).__init__(sockfile=HOST_SOCK)
