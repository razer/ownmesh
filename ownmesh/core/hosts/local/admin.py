from django.contrib import admin

# from .models import (Local, LocalConfig, IoPin, IoInput, IoOutput,
from .models import (Local, IoPin, IoInput, IoOutput,
                     IoOscillator, IoPulse)
from .forms import IoPinForm
from core.hosts.admin import BaseHostAdmin


@admin.register(Local)
class LocalAdmin(BaseHostAdmin):
    plugin_fields = ('gpio',)


@admin.register(IoPin)
class IoPinAdmin(admin.ModelAdmin):
    form = IoPinForm


@admin.register(IoInput)
class IoInputAdmin(IoPinAdmin):
    pass


@admin.register(IoOutput)
class IoOutputAdmin(IoPinAdmin):
    pass


admin.site.register(IoOscillator)
admin.site.register(IoPulse)
