import logging
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.core.exceptions import FieldError
from core.hosts.models import Host
from core.datas.models import Data
from values import local as V
from .gpio.pin import PiPin, BbPin

logger = logging.getLogger(__name__)


def get_pinchoices():
    if not Local.objects.count():
        return None
    gpio = Local.objects.first().gpio
    if gpio == V.DISABLED:
        return None
    if gpio == V.RPI:
        return V.RPIPINS_CHOICES
    if gpio == V.BEAGLEBONE:
        return V.BBPINS_CHOICES
    return None


class Local(Host):
    gpio = models.CharField(
        max_length=2, choices=V.GPIO_CHOICES, default=V.DISABLED)

    def __str__(self):
        return self.name

    def get_connector(self):
        pass

    def get_iopin_class(self):
        if self.gpio in V.BEAGLEBONE:
            try:
                _ = __import__('Adafruit_BBIO.GPIO')  # pylint: disable=W0612
                return BbPin
            except Exception:
                logger.error('Loading Adafruit_BBIO python library failed')
        elif self.gpio in V.RPI:
            try:
                _ = __import__('RPi.GPIO')  # pylint: disable=W0612
                return PiPin
            except Exception:
                logger.error('Loading RPi GPIO python library failed')
        return None

    def send(self, payload=b'', ping=False, check=False, overwritten=False):
        pass

    def ping(self):
        pass

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Local, self).save()
        # Ensure signals are triggered
        h = Host.objects.get(id=self.id)
        h.save()


class IoOscillator(models.Model):
    period = models.IntegerField(default=1000)
    cyclicrate = models.FloatField(
        validators=[MinValueValidator(0), MaxValueValidator(1)], default=0.5)

    def __str__(self):
        return ('Period %s millis, Cyclic rate %s/1'
                % (self.period, self.cyclicrate))


class IoPulse(models.Model):
    length = models.IntegerField(default=200)

    def __str__(self):
        return 'Pulse %s millis' % self.length


class IoPin(models.Model):
    data = models.OneToOneField(Data, related_name='iopin', default=None,
                                on_delete=models.CASCADE)
    pin = models.CharField(default='0', max_length=5)
    inverted_logic = models.BooleanField(default=False)

    @property
    def child(self):
        for child_model in 'ioinput', 'iooutput':
            if hasattr(self, child_model):
                return getattr(self, child_model)
        raise FieldError('Iopin with no child !!!')

    @property
    def pinname(self):
        try:
            return dict(get_pinchoices())[self.pin]
        except Exception:
            return V.DISABLED

    @property
    def pinmode(self):
        return 'Output' if hasattr(self, 'iooutput') else 'Input'

    def __str__(self):
        details = f'{self.pinmode} | {self.pinname} | {self.child.str_repr}'
        if self.inverted_logic:
            details += ' | Inverted logic'
        return f'{details} | {self.data.shortname}'


class IoInput(IoPin):
    host = models.ForeignKey(Local, related_name='ioinput', default=None,
                             on_delete=models.CASCADE)
    pullup = models.BooleanField(default=False)
    detect = models.CharField(max_length=2,
                              choices=V.DETECTION_CHOICES,
                              default=V.BOTH)
    read_freq = models.FloatField(default=0.2)

    @property
    def str_repr(self):
        details = 'Pullup' if self.ioinput.pullup else 'Pulldown'
        details += f', Detect on {self.get_detect_display()}'
        return details


class IoOutput(IoPin):
    host = models.ForeignKey(Local, related_name='iooutput', default=None,
                             on_delete=models.CASCADE)
    oscillator = models.ForeignKey(
        IoOscillator, related_name='iooutput', null=True, blank=True,
        on_delete=models.CASCADE)
    pulse = models.ForeignKey(
        IoPulse, related_name='iooutput', null=True, blank=True,
        on_delete=models.CASCADE)

    @property
    def str_repr(self):
        if self.oscillator:
            return (f'Oscillator {self.oscillator.cyclicrate}' +
                    f':{self.oscillator.period}ms')
        if self.pulse:
            return f'Pulse {self.pulse.length}ms'
        return None
