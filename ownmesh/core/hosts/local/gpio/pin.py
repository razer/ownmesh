import logging
logger = logging.getLogger(__name__)


class Pin:

    def __init__(self, pin_model):
        self.pin_model = pin_model
        self.io = None
        self.pin = None
        self.pinmode = None
        self.pullup = False

    def setup(self):
        if self.pin_model:
            self.pin = self.pin_model.pin
            self.pinmode = self.io.OUT
            self.pullup = None
            if hasattr(self.pin_model, 'ioinput'):
                self.pinmode = self.io.IN
                self.pullup = self.pin_model.pullup

    def read(self):
        return self.io.input(self.pin)

    def write(self, value, log=True):
        if self.pinmode is not self.io.OUT:
            if log:
                logger.error(f'GPIO Pin : Writing to {self.pin}: is input !')
            return
        if log:
            logger.info(f'GPIO Pin : Set {self.pin} to {bool(value)}')
        self.io.output(self.pin, int(value))

    def cleanup(self, pin=None):
        pass


class PiPin(Pin):

    def __init__(self, pin_model):
        super(PiPin, self).__init__(pin_model)
        self.io = __import__('RPi')
        self.io = self.io.GPIO
        self.io.setmode(self.io.BOARD)
        self.setup()

    def setup(self):
        super(PiPin, self).setup()
        if not hasattr(self, 'pin'):
            return
        self.pin = int(self.pin)
        if self.pullup is None:
            self.io.setup(self.pin, self.pinmode)
            return
        self.io.setup(self.pin, self.pinmode, pull_up_down=self.pullup + 21)

    def cleanup(self, pin=None):
        self.io.setup(int(pin), self.io.IN, pull_up_down=self.io.PUD_DOWN)


class BbPin(Pin):

    def __init__(self, pin_model):
        super(BbPin, self).__init__(pin_model)
        self.io = __import__('Adafruit_BBIO').GPIO
        self.setup()

    def setup(self):
        super(BbPin, self).setup()
        if not hasattr(self, 'pin'):
            return
        if self.pullup is None:
            # Delay permit to fix permissions before pin setup
            self.io.setup(self.pin, self.pinmode, delay=200)
            return
        self.io.setup(self.pin, self.pinmode, self.pullup, delay=200)

    def cleanup(self, pin=None):
        self.io.setup(int(pin), self.io.IN, self.io.PUD_DOWN)
