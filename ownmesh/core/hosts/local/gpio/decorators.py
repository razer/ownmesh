from .utils import gpio_enabled

import logging
logger = logging.getLogger(__name__)


def gpio_filter(created=None):
    def _gpio_filter(func):
        def wrapper(*args, **kwargs):
            if not gpio_enabled():
                return
            if created is not None and 'created' in kwargs.keys():
                if kwargs['created'] is not created:
                    return
            func(*args, **kwargs)
        return wrapper
    return _gpio_filter
