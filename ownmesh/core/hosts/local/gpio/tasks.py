import sys
import signal
import asyncio
import logging
from celery import task

# from core.hosts.local.models import Local
# from values import local as V
from values.datas import RECEIVED
from .utils import get_host, gpio_enabled
from .connector import (Connector,
                        GpioSock as GS)

logger = logging.getLogger(__name__)

@task(queue='services')
def check_service_task(init=False):
    if not gpio_enabled():
        return
    if not init:
        try:
            recv = GS().send(GS.STATUS + GS.ENDLINE, response=True)
            if recv:
                if GS.RUNNING in recv:
                    logger.info('GPIO connector is running')
                return
        except Exception:
            logger.info(sys.exc_info()[1])
            return

    logger.info('Starting GPIO connector...')
    connector = Connector(loop=asyncio.new_event_loop(),
                          input_cb=gpio_input_task)
    signal.signal(signal.SIGINT, connector.signal_handler)
    connector.start()


@task(queue='services')
def gpio_input_task(pin=None, value=None):
    if not gpio_enabled():
        return
    if value is None or pin is None:
        return
    local = get_host()
    ioinput = local.ioinput.get(pin=pin)
#     data_filter = local.data.filter(header=header)
#     if not data_filter.count():
#         return
#     data = data_filter.first()
    value = not value if ioinput.inverted_logic else bool(value)
    logger.info(f'{ioinput.pinname} : Gpio input change')
    ioinput.data.set_value(value, direction=RECEIVED)


@task(queue='services')
def gpio_output_task(pin=None):
    if not gpio_enabled():
        return
    local = get_host()
    iooutput_filter = local.iooutput.filter(pin=pin)
    if not iooutput_filter.count():
        return
    iooutput = iooutput_filter.first()
#     data = local.data.get(header=header)
    value = iooutput.data.latest_value
    if value is None:
        value = 0
    value = int(not value) if iooutput.inverted_logic else int(value)
    logger.info(f'{iooutput.pinname} (Output) -> set new value : {value}')

    GS().send(GS.OUTPUT_CHANGE + GS.DELIMIT +
              bytearray(str(iooutput.pin), 'utf-8') +
              GS.DELIMIT + bytearray(str(value), 'utf-8'))
