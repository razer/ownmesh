from core.hosts.local.models import Local
from values.local import DISABLED

import logging
logger = logging.getLogger(__name__)


def get_host():
    if not Local:
        return
    return Local.objects.get_or_create()[0]


def gpio_enabled():
    host = get_host()
    if not host:
        return
    if host.gpio in DISABLED:
        return
    return True
