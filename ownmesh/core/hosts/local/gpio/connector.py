import os
import asyncio
from django.db import connection
from threading import Thread
from contextlib import suppress

from .utils import get_host
from project.settings import GPIO_SOCK
from core.hosts.sock import UnixSock

import logging
logger = logging.getLogger(__name__)


class GpioSock(UnixSock):
    INPUT_CHANGE = b'INPUTCHANGE'
    NEW_OUTPUT = b'NEWOUTPUT'
    DELETED_OUTPUT = b'DELETEDOUTPUT'
    EDITED_INPUT = b'EDITEDINPUT'
    EDITED_OUTPUT = b'EDITEDOUTPUT'
    OUTPUT_CHANGE = b'OUTPUTCHANGE'
    EXIT = b'EXIT'

    def __init__(self):
        super(GpioSock, self).__init__(sockfile=GPIO_SOCK)


GS = GpioSock  # @IgnorePep8


class Connector(Thread):

    def __init__(self, loop=None, input_cb=None):
        super(Connector, self).__init__()
        self.daemon = True
        self.loop = loop
        self.input_cb = input_cb
        self.host = get_host()
        with suppress(OSError):
            os.remove(GPIO_SOCK)
        self.pin_class = self.host.get_iopin_class()
        self.reader_map = []
        self.writer_map = []
        self.need_update_map = []
        self.update()
        self.end_loop = False

    def update(self, pin_map=None):
        # Very important : avoid 'MySQL server has gone away' error
        connection.close_if_unusable_or_obsolete()
        if pin_map is None or 'input' in pin_map:
            self.input_map = self.host.ioinput.all()
        if pin_map is None or 'output' in pin_map:
            self.output_map = self.host.iooutput.all()

    async def read_pin(self, pin_model, init=False):
        pin_model.refresh_from_db()
        pin = self.pin_class(pin_model)
        previous_value = pin.read()
        mode = 'pullup' if pin_model.pullup else 'pulldown'
        logger.info('Input %s -> starting reader, mode %s, initial value %s' %
                    (pin_model, mode, previous_value))
        self.input_cb.s(
            pin=pin_model.pin, value=previous_value).delay()
        while True:
            if pin_model not in self.input_map:
                logger.info(
                    '%s (Input) was removed, stopping reader' % pin_model)
                self.reader_map.remove(pin_model.pin)
                pin.cleanup()
                break
            if pin_model.pin in self.need_update_map:
                logger.info(
                    '%s (Input) need to be updated, stopping reader'
                    % pin_model)
                self.reader_map.remove(pin_model.pin)
                pin.cleanup()
                break
            value = pin.read()
            if value is not previous_value:
                previous_value = value
                logger.info('%s (Input) -> get new value %s' %
                            (pin_model, value))
                self.input_cb.s(pin=pin_model.pin, value=value).delay()
            await asyncio.sleep(pin_model.read_freq)

        logger.info('%s : removing from reader_map' % pin_model)
        self.reader_map.remove(pin_model.pin)

    async def pulse_pin(self, pin, length):
        pulse_done = False
        while True:
            if pulse_done:
                pin.write(0)
                break
            pulse_done = True
            pin.write(1)
            await asyncio.sleep(float(length / 1000))

    async def write_pin(self, pin_model, value, init=False):
        pin = self.pin_class(pin_model)
        oscillator = None
        if pin_model.oscillator:
            real_value = None
            oscillator = pin_model.oscillator
            sleep_time_on = oscillator.period * oscillator.cyclicrate / 1000
            sleep_time_off = oscillator.period * \
                (1 - oscillator.cyclicrate) / 1000

        while True:
            if pin_model not in self.output_map:
                logger.info('%s (Output) is now disabled' % pin_model)
                pin.cleanup()
                break
            if pin_model.pin in self.need_update_map:
                logger.info(
                    '%s (Output) need to be updated, stopping writer'
                    % pin_model)
                break
            if pin_model.pulse:
                if not init:
                    logger.info('%s (Output) : pulsing now' % pin_model)
                    self.loop.create_task(
                        self.pulse_pin(pin, pin_model.pulse.length))
                break
            if oscillator is None or not value:
                logger.info('%s (Output) : writing value %s' %
                            (pin_model, value))
                pin.write(value)
                break
            real_value = 1 if real_value is None else int(not real_value)
            pin.write(real_value, log=False)
            sleep_time = sleep_time_on if real_value else sleep_time_off
            await asyncio.sleep(sleep_time)

        logger.info('%s : removing from writer_map' % pin_model)
        self.writer_map.remove(pin_model.pin)

    async def reader_reload(self, pin_model, init=True):
        logger.info('%s : reloading reader now' % pin_model)
        while True:
            if pin_model.pin not in self.reader_map:
                break
            await asyncio.sleep(0.1)
        self.reader_map.append(pin_model.pin)
        self.need_update_map.remove(pin_model.pin)
        self.loop.create_task(self.read_pin(pin_model, init=init))
        logger.info('%s : reloading done' % pin_model)

    async def writer_reload(self, pin_model, value, init=True):
        logger.info('%s : reloading writer now' % pin_model)
        while True:
            if pin_model.pin not in self.writer_map:
                break
            await asyncio.sleep(0.1)
        self.writer_map.append(pin_model.pin)
        self.need_update_map.remove(pin_model.pin)
        self.loop.create_task(self.write_pin(pin_model, value, init=init))
        logger.info('%s : reloading done' % pin_model)

    async def sock_handler(self, reader, writer):
        payload = await reader.readline()
        # Very important : avoid 'MySQL server has gone away' error
        connection.close_if_unusable_or_obsolete()
        if GS.EXIT in payload:
            self.exit()
        if GS.STATUS in payload:
            logger.debug('GPIO Connector : Send status response')
            writer.write(GS.RUNNING + GS.ENDLINE)
            await writer.drain()
            writer.close()
            return
        if GS.NEW_OUTPUT in payload or GS.DELETED_OUTPUT in payload:
            self.update(pin_map='output')
            self.update_writers()
            return

        payload_split = payload.split(GS.DELIMIT)
        if len(payload_split) < 2:
            return

        pin = payload_split[1].decode('utf-8')

        if GS.INPUT_CHANGE in payload:
            logger.info('Input change : %s' % payload)
            ioinput_filter = self.host.ioinput.filter(pin=pin)
            if not ioinput_filter.count():
                logger.info(
                    'Trying to edit input pin %s : model not found' % pin)
                return
            pin_model = ioinput_filter.first()
            self.need_update_map.append(pin_model.pin)
            self.loop.create_task(
                self.reader_reload(pin_model, init=False))
            return

        value = payload_split[2].decode('utf-8')
        if not len(value) or not len(pin):
            logger.info('Bad payload : %s' % payload)
            return

        value = int(value)
        iooutput_filter = self.host.iooutput.filter(pin=pin)
        if not iooutput_filter.count():
            logger.info(
                'Trying to write/edit output pin %s : model not found' % pin)
            return
        pin_model = iooutput_filter.first()

        if GS.EDITED_OUTPUT in payload:
            self.need_update_map.append(pin)
            self.loop.create_task(self.writer_reload(pin_model, value))

        value = payload_split[2]
        if not len(value):
            logger.info('Bad payload : %s' % payload)
            return
        value = int(value.decode('utf-8'))
        if GS.OUTPUT_CHANGE in payload:
            logger.info('Output change : %s' % payload)
            self.need_update_map.append(pin_model.pin)
            self.loop.create_task(
                self.writer_reload(pin_model, value, init=False))

    def sock_handler_cb(self, reader, writer):
        self.loop.create_task(self.sock_handler(reader, writer))

    def update_readers(self):
        logger.debug('Update readers now')
        for pin_model in self.input_map:
            if pin_model.pin in self.reader_map:
                continue
            logger.debug('New input pin : %s' % pin_model)
            self.reader_map.append(pin_model.pin)
            self.loop.create_task(self.read_pin(pin_model))

    def update_writers(self, init=True):
        logger.debug('Update writers now')
        for pin_model in self.output_map:
            if pin_model.pin in self.writer_map:
                continue
            value = pin_model.data.latest_value
            if value is None:
                logger.warning('%s : no value found, assuming 0' % pin_model)
                value = 0
            self.writer_map.append(pin_model.pin)
            self.loop.create_task(self.write_pin(pin_model, value, init=init))

    def signal_handler(self, signal, frame):
        self.exit()

    def exit(self):
        logger.info('Stopping GPIO asyncio event loop')
        self.loop.stop()
        self.sock_server.close()
        for input_pin in self.reader_map + self.writer_map:
            self.pin_class(None).cleanup(input_pin)
        os.remove(GPIO_SOCK)
#         sys.exit(0)

#     async def test(self):
#         while True:
#             logger.info('GPIO event loop test')
#             await asyncio.sleep(30)

    def run(self):
        asyncio.set_event_loop(self.loop)
        self.sock_server = asyncio.start_unix_server(
            self.sock_handler_cb, path=GPIO_SOCK, loop=self.loop)
        self.loop.run_until_complete(self.sock_server)
#         self.loop.create_task(self.test())
        self.update_readers()
        self.update_writers(init=True)
        logger.info('GPIO asyncio loop ready')
        self.loop.run_forever()
