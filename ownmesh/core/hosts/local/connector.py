import socket
from project.settings import GPIO_SOCK
# Logger
import logging
logger = logging.getLogger(__name__)


class Connector:

    def __init__(self, config):
        self.config = config

    def send(self, payload):
        sock = socket.socket(socket.AF_UNIX)
        sock.connect(GPIO_SOCK)
        sock.send(payload)
        sock.close()
        pass

    def ping(self):
        pass
