# import sys
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .gpio.decorators import gpio_filter
from .models import Local, IoInput, IoOutput
from .gpio.connector import GpioSock as GS
from .gpio.tasks import check_service_task, gpio_output_task
from values.local import DISABLED

import logging
logger = logging.getLogger(__name__)


@receiver(post_save, sender=Local)
def local_change(sender, **kwargs):
    local = kwargs['instance']
    if local.gpio == DISABLED:
        try:
            GS().send(GS.EXIT)
            return
        except Exception:
            None
    try:
        recv = GS().send(GS.STATUS + GS.ENDLINE, response=True)
        if recv:
            if GS.RUNNING in recv:
                GS().send(GS.EXIT)
    except Exception:
        None
    check_service_task.s(  # @UndefinedVariable
        init=True).apply_async(countdown=3)  # @UndefinedVariable


@receiver(post_save, sender=IoInput)
@receiver(post_delete, sender=IoInput)
@gpio_filter()
def input_change(sender, **kwargs):
    ioinput = kwargs['instance']
    logger.info('Signal from %s -> input_change()' % ioinput)
    GS().send(GS.INPUT_CHANGE + GS.DELIMIT +
              bytearray(str(ioinput.pin), 'utf-8') +
              GS.DELIMIT)


@receiver(post_save, sender=IoOutput)
@gpio_filter(created=True)
def output_new(sender, **kwargs):
    iooutput = kwargs['instance']
    logger.info('Signal from %s -> output_new()' % iooutput)
    GS().send(GS.NEW_OUTPUT)


@receiver(post_save, sender=IoOutput)
@gpio_filter(created=False)
def output_edited(sender, **kwargs):
    iooutput = kwargs['instance']
    logger.info('Signal from %s -> output_edited()' % iooutput)
    gpio_output_task.s(pin=iooutput.pin).delay()  # @UndefinedVariable


@receiver(post_delete, sender=IoOutput)
@gpio_filter()
def output_deleted(sender, **kwargs):
    iooutput = kwargs['instance']
    logger.info('Signal from %s -> output_deleted()' % iooutput)
    GS().send(GS.DELETED_OUTPUT)
