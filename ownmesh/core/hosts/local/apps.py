from django.apps import AppConfig

import logging
logger = logging.getLogger(__name__)


class LocalConfig(AppConfig):
    name = 'core.hosts.local'

    def ready(self):
        logger.debug('Local app signals init')
        from core.hosts.local import signals as localsignals  # @UnusedImport
