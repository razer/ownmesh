from django import forms
from .models import Local, IoPin
from values import local as V

import logging
logger = logging.getLogger(__name__)


class IoPinForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(IoPinForm, self).__init__(*args, **kwargs)
        self.fields['pin'] = forms.ChoiceField(choices=self.get_pinchoices())

    def get_pinchoices(self):
        if not Local.objects.count():
            return None
        gpio = Local.objects.first().gpio
        if gpio == V.DISABLED:
            return None
        choices = None
        if gpio == V.RPI:
            choices = V.RPIPINS_CHOICES
        if gpio == V.BEAGLEBONE:
            choices = V.BBPINS_CHOICES
        if not choices:
            return None
        choices = list(choices)
        for iopin in IoPin.objects.exclude(pk=self.instance.pk):
            for choice in choices:
                if iopin.pin == choice[0]:
                    choices.remove(choice)
        return tuple(choices)
