import logging
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from core.hosts.models import Host
from core.hosts.local.models import Local
from core.datas.models import NetStatus
from api.serializers import HostSerializer
from frontend.consumers import sse_push
import values.datas as VD

logger = logging.getLogger(__name__)


@receiver(pre_delete, sender=Host)
def on_host_deleted(sender, **kwargs):
    host = kwargs['instance']
    host_serializer = HostSerializer(Host.objects.all(), many=True)
    sse_push('host', host_serializer.data, many=True)
    if isinstance(host.get_model(), Local):
        return
    if not hasattr(host, 'netstatus_data'):
        return
    host.netstatus_data.delete()


@receiver(post_save, sender=Host)
def on_host_saved(sender, **kwargs):
    host = kwargs['instance']
    sse_push('host', HostSerializer(host).data)
    if isinstance(host.get_model(), Local) or hasattr(host, 'netstatus_data'):
        return
    logger.info(f'{host.name} have no netstatus data, creating it')
    assert Local.objects.count(), 'Local host not found !'
    # if not Local.objects.count():
    #     logger.error('No local host found !!!')
    #     return
    localhost = Local.objects.first()
    NetStatus.objects.create(host=localhost, related_host=host,
                             entry_type=VD.BOOL,
                             entry_creation=VD.ONLYNEWVALUE,
                             role=VD.NETSTATUS,
                             shortname=f'Netstatus: {host.name}',
                             name=f'Online status for {host.name}')
