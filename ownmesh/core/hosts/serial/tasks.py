import logging
from celery import task
from core.hosts.serial.models import Serial

logger = logging.getLogger(__name__)


def _get_instance(port):
    f = Serial.objects.filter(port=port)
    if not f.count():
        logger.error('Serial host with port %s not found', port)
        return None
    if f.count() != 1:
        logger.warning(
            'Serial host : multiple data model for %s, taking first', port)
    return f[0]


@task(queue='services')
def on_new_serialrx_task(port, payload):
    host = _get_instance(port)
    if not host:
        return True
    host.get_payload(payload)
    return False


@task(queue='services')
def on_port_init_task(port):
    host = _get_instance(port)
    if not host:
        return True
    host.set_netstatus(True)
    return False


@task(queue='services')
def on_port_close_task(port):
    host = _get_instance(port)
    if not host:
        return True
    host.set_netstatus(False)
    return False
