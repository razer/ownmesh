from django.apps import AppConfig


class SerialConfig(AppConfig):
    name = 'core.hosts.serial'
