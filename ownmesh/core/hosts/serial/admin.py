from django.contrib import admin
from .models import SerialConfig, Serial
from core.hosts.admin import BaseHostAdmin, HostPluginConfigAdmin


@admin.register(SerialConfig)
class SerialConfigAdmin(HostPluginConfigAdmin):
    plugin_fields = ('active_ports',)
    plugin_readonly_fields = ('active_ports',)


@admin.register(Serial)
class SerialAdmin(BaseHostAdmin):
    plugin_fields = ('port', 'baudrate')
