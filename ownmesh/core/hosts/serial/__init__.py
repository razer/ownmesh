from __future__ import absolute_import
from core.hosts.config import HOSTTYPES, SERIAL

import logging
logger = logging.getLogger(__name__)

default_app_config = 'core.hosts.serial.apps.SerialConfig'

serial = None  # @UnusedVariable

try:
    import serial
except Exception:
    if SERIAL in HOSTTYPES:
        logger.error(
            'Python Serial module not found, Serial support is disabled')
        logger.error('Please install python-serial')
