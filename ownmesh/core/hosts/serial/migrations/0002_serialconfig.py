# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-03-22 11:13
from __future__ import unicode_literals

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('serial', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SerialConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('enabled', models.BooleanField(default=False)),
                ('ping_retry', models.IntegerField(default=30)),
                ('tx_wait', models.IntegerField(default=2)),
                ('active_ports', django.contrib.postgres.fields.ArrayField(base_field=models.TextField(max_length=16), size=None)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
