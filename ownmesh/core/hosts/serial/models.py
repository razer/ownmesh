from django.db import models
from django.contrib.postgres.fields import ArrayField
from core.hosts.models import Host, HostPluginConfig

import logging
logger = logging.getLogger(__name__)


class SerialConfig(HostPluginConfig):
    active_ports = ArrayField(
        models.TextField(max_length=16, null=True, blank=True),
        default=[])


class Serial(Host):
    port = models.TextField(default='/dev/ttyS0', max_length=16)
    baudrate = models.IntegerField(default=9600)

    def __str__(self):
        return '%s (%s)' % (self.name, 'Serial')

    def get_parent(self):
        return Host.objects.get(id=self.id)

    def get_sendheader(self):
        return self.port

    def save(self, **kwargs):
        super(Serial, self).save(**kwargs)
        h = Host.objects.get(id=self.id)
        h.save(**kwargs)
