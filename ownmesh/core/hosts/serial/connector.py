import sys
import logging
import asyncio
from values.hosts import FRAME as F
from core.hosts.utils import tobytes
from core.hosts.pluginConnector import PluginConnector
from . import serial
from .tasks import on_new_serialrx_task

logger = logging.getLogger(__name__)

class Connector(PluginConnector):

    def __init__(self, loop=None, config=None):
        super().__init__(loop, config)
        self.serial = {}

    def begin(self):
        '''
            Initialize plugin
        '''
        super().begin()
        for port_spec in self.config.active_ports:
            port, baudrate = port_spec.split(':')
            self.serial[port] = serial.Serial(port=port)
            self.serial[port].baudrate = int(baudrate)
            self.serial[port].timeout = 1.0
            if not self.serial[port].isOpen():
                try:
                    self.serial[port].open()
                except Exception:
                    logger.error('Failed to open serial port %s : %s',
                                 port, sys.exc_info()[0])
                    self.serial.pop(port)
                    continue
#             on_port_init_task.s(port).delay()  # @UndefinedVariable
            logger.info('Serial init of port %s with baudrate %s done',
                        port, baudrate)
        self.read_task = self.loop.create_task(self.read())
        return True

    def write(self, address, payload):
        '''
            Send to host bytes payload
        '''
        port = address
        try:
            if not self.serial[port].isOpen():
                return
            self.serial[port].write(tobytes(payload) + F.NEWLINE)
        except Exception:
            logger.error('Serial send : %s', sys.exc_info()[1])

    async def read(self):
        '''
            Read/Listen for data
        '''
        try:
            while True:
                for ser in self.serial.values():
                    if not ser.in_waiting:
                        continue
                    payload = ser.readline()
                    logger.debug('Serial rx payload : %s', payload)
                    on_new_serialrx_task.s(ser.port, payload).delay()
                await asyncio.sleep(self.read_delay)
        except Exception:
            logger.error('Serial read : %s', sys.exc_info()[1])
