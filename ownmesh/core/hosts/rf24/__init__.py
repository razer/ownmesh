from __future__ import absolute_import
# from core.hosts.config import HOSTTYPES, RF24
#
# import logging
# logger = logging.getLogger(__name__)

default_app_config = 'core.hosts.rf24.apps.Rf24Config'

# RF24 = None
# RF24Network = None
# RF24NetworkHeader = None
# RF24_250KBPS = None
#
# try:
#     from RF24 import *
#     from RF24Network import *
# except Exception:
#     if RF24 in HOSTTYPES:
#         logger.error(
#             'No library found for RF24 radio modules, support is disabled')
#         logger.error('Please install python RF24 bindings')
