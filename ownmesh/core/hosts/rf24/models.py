import logging
from django.db import models  # @UnusedImport
# from core.hosts.rf24 import *  # @UnusedWildImport
from core.hosts.models import HostPluginConfig, Host
# from .connector import Connector

logger = logging.getLogger(__name__)


class Rf24Config(HostPluginConfig):
    spi_dev = models.IntegerField(default=0)
    ce_pin = models.IntegerField(default=22)
    channel = models.IntegerField(default=80)
    max_streamsize = models.IntegerField(default=64)


class Rf24(Host):
    address = models.TextField(default='00', max_length=23)

    def __str__(self):
        return '%s (%s)' % (self.name, self.address)

    def get_sendheader(self):
        return self.address

    def save(self, **kwargs):
        super(Rf24, self).save(**kwargs)
        # Ensure signals are triggered
        h = Host.objects.get(id=self.id)
        h.save(**kwargs)
