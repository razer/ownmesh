import logging
from celery import task

from core.hosts.models import Host
from .models import Rf24

logger = logging.getLogger(__name__)


@task(queue='services')
def on_new_rf24rx_task(address, stream):
    if not Rf24.objects.filter(address=address).count():
        logger.error(f'Rf24 host with address {address} not found')
        return True
    host = Host.objects.get(pk=Rf24.objects.get(address=address).pk)
    host.get_payload(stream)
