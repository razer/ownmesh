from . import RF24, RF24Network, RF24_250KBPS


def rf24_init(Rf24Config):
    config, created = Rf24Config.objects.get_or_create()  # @UnusedVariable
    radio = RF24(config.ce_pin,
                 config.spi_dev)
    try:
        if radio.begin():
            radio.setDataRate(RF24_250KBPS)
            network = RF24Network(radio)
            network.begin(config.channel, 0)
            return radio, network
        return (None, None)
    except:
        return (None, None)
