from django.contrib import admin
from core.hosts.admin import BaseHostAdmin, HostPluginConfigAdmin
from .models import Rf24Config, Rf24

import logging
logger = logging.getLogger(__name__)


@admin.register(Rf24Config)
class Rf24ConfigAdmin(HostPluginConfigAdmin):
    plugin_fields = ('spi_dev', 'ce_pin', 'channel', 'max_streamsize')


@admin.register(Rf24)
class Rf24Admin(BaseHostAdmin):
    plugin_fields = ('address',)
