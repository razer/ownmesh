from django.apps import AppConfig


class Rf24Config(AppConfig):
    name = 'core.hosts.rf24'
