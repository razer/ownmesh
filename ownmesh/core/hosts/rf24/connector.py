import asyncio
import sys
import logging

# from . import RF24, RF24Network, RF24_250KBPS, RF24NetworkHeader
from RF24 import RF24, RF24_250KBPS, RF24_PA_MAX  # pylint: disable=import-error
from RF24Network import RF24Network, RF24NetworkHeader  # pylint: disable=import-error

from core.hosts.pluginConnector import PluginConnector
from core.hosts.utils import tobytes
from values.hosts import FRAME as F
from .tasks import on_new_rf24rx_task

logger = logging.getLogger(__name__)


class Connector(PluginConnector):

    def __init__(self, *args, **kwargs):
        super(Connector, self).__init__(*args, **kwargs)
        self.radio = RF24(self.config.ce_pin,
                          self.config.spi_dev)
        self.network = None
        self.ready = False

    def begin(self):
        '''
            Initialize plugin
        '''
        super(Connector, self).begin()
        try:
            if not self.radio.begin():
                logger.error(self.error)
                return False
            self.radio.setPALevel(RF24_PA_MAX)
            self.radio.setDataRate(RF24_250KBPS)
            self.network = RF24Network(self.radio)
            self.network.begin(self.config.channel, 0)
            logger.info('Rf24 plugin ready')
            self.read_task = self.loop.create_task(self.read())
            self.ready = True
            return True
        except Exception:
            logger.error(self.error)
            logger.error(sys.exc_info()[1])
            return False

    # @property
    # def ready(self):
    #     if self.network:
    #         return True
    #     if self.show_error:
    #         logger.error('Rf24 not ready : did you begin() ?')
    #         self.show_error = False
    #     return False

    def write(self, address, payload):
        '''
            Send to host bytes payload
        '''
        if not self.ready:
            logger.error('RF24 not ready !')
            return
        try:
            address = int(address, 8)
            self.network.write(RF24NetworkHeader(address),
                               tobytes(payload) + F.CRETURN)
        except Exception:
            logger.error(f'RF24 send : {sys.exc_info()[1]}')

    async def read(self):
        '''
            Read/Listen for data
        '''
        if not self.ready:
            return
        try:
            while True:
                self.network.update()
                if not self.network.available():
                    await asyncio.sleep(self.read_delay)
                header, payload = self.network.read(self.config.max_streamsize)
                address = format(header.from_node, 'o')
                for line in payload.split(F.CRETURN):
                    if not line:
                        continue
                    line = bytes(line)
                    logger.info(f'Received {line} from addr {address}')
                    on_new_rf24rx_task.delay(  # @UndefinedVariable
                        address, line)
        except Exception:
            logger.error(f'RF24 send : {sys.exc_info()[1]}')
