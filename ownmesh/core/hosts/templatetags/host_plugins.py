from django.template import Library
from core.hosts.config import HOSTTYPES, LOCAL
register = Library()


# @register.filter(name='hosttypes')
@register.simple_tag
def hosttypes_list():
    hosttypes = HOSTTYPES.copy()
    hosttypes.remove(LOCAL)
    return hosttypes


@register.filter(name='hosttype_url')
def hosttype_url(hosttype):
    return f'../../{hosttype}/{hosttype}/add'
