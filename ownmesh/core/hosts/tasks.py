import sys
import signal
import asyncio
import logging
from celery import task

from core.datas.models import NetStatus
from values.hosts import DEEP_SLEEP
from values.datas import RECEIVED

from .connector import Connector
from .sock import HostSock as HS
from .models import Host
from .config import LOCAL, HOSTTYPES
from .utils import get_host_config

logger = logging.getLogger(__name__)


@task(queue='services')
def ping_check_task(nsdataid):
    nsdata = NetStatus.objects.get(pk=nsdataid)
    if nsdata.wait_response:
        if nsdata.latest_value is False:
            return False
        nsdata.set_value(False, direction=RECEIVED)
        nsdata.wait_response = False
        nsdata.save()
    return None


@task(queue='services')
def check_connector_task(init=False):
    '''
        Check for a working connector
        Start it otherwise
    '''
    if not init:
        try:
            recv = HS().send(HS.STATUS, response=True)
            if recv:
                if HS.RUNNING in recv:
                    logger.info('Host connector is running')
                    return
        except Exception:
            logger.error(sys.exc_info()[1])
            return

    connector = Connector(loop=asyncio.new_event_loop())
    signal.signal(signal.SIGINT, connector.signal_handler)
    connector.start()


def _get_hosts():
    running_connectors = []
    hostlist = []
    for hosttype in HOSTTYPES:
        if LOCAL in hosttype:
            continue
        host_config = get_host_config(hosttype)
        if not host_config.enabled:
            continue
        if not host_config.running:
            continue
        running_connectors.append(hosttype)

    for host in Host.objects.filter(enabled=True):
        if host.get_type() not in running_connectors:
            continue
        hostlist.append(host.get_model())
    return hostlist


@task(queue='services')
def host_check_task(pk=None):
    def host_check(host):
        if host.power_mode in DEEP_SLEEP:
            return
        host.check_netstatus()
        ping_check_task.s(host.netstatus_data.pk).apply_async(countdown=30)
    if pk is not None:
        host_check(Host.objects.get(pk=pk))
        return
    for host in _get_hosts():
        host_check(host)


# @task(queue='services')
# def hosts_check_task():
#     for host in _get_hosts():
        # if host.power_mode in DEEP_SLEEP:
        #     continue
        # host.check_netstatus()
        # ping_check_task.s(host.netstatus_data.pk).apply_async(countdown=30)


@task(queue='services')
def host_init_task(pk=None):
    if pk is not None:
        Host.objects.get(pk=pk).set_values(clock=True)
        return
    for host in _get_hosts():
        host.set_values(clock=True)


# @task(queue='services')
# def hosts_init_task():
#     for host in _get_hosts():
#         host.set_values(clock=True)


@task(queue='services')
def host_set_values_task(pk=None, clock=False, onlypending=True):
    Host.objects.get(pk=pk).set_values(clock=clock,
                                       onlypending=onlypending)


@task(queue='services')
def hosts_set_values_task(clock=False, onlypending=True):
    for host in _get_hosts():
        host.set_values(clock=clock, onlypending=onlypending)


@task(queue='services')
def set_clock_task(pk=None):
    if pk is not None:
        Host.objects.get(pk=pk).set_clock()
        return
    for host in _get_hosts():
        host.set_clock()


# @task(queue='services')
# def set_clocks_task():
#     for host in _get_hosts():
#         host.set_clock()


@task(queue='services')
def set_pending_values_task(pk=None):
    if pk is not None:
        Host.objects.get(pk=pk).set_values(onlypending=True)
        return
    for host in _get_hosts():
        host.set_values(onlypending=True)
