from django.utils.translation import ugettext_lazy as _

LOCAL = 'local'
XBEE = 'xbee'
SERIAL = 'serial'
TCPSOCKET = 'tcpsocket'
RF24 = 'rf24'
HOSTTYPES = [LOCAL, XBEE, SERIAL, TCPSOCKET, RF24]
# HOSTTYPES=[LOCAL,SERIAL,TCPSOCKET,RF24]
# HOSTTYPES = [LOCAL]

HOSTPLUGINS_CHOICES = (
    (XBEE, _('Support for xbee hosts')),
    (SERIAL, _('Support for serial hosts')),
    (TCPSOCKET, _('Support for tcp/ip hosts')),
    (RF24, _('Support for nrf24 hosts')),
)

HOSTADD_CHOICES = (
    (XBEE, _('Xbee host')),
    (SERIAL, _('Serial host')),
    (TCPSOCKET, _('Tcp/ip host')),
    (RF24, _('Nrf24 host')),
)

# READPERIOD_CHOICES = (
#     (.2, 'Fast : 5 reads/second'),
#     (.5, 'Medium : 2 reads/second'),
#     (1, 'Slow : 1 read/second'),)
