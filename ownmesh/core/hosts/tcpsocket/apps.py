from django.apps import AppConfig


class TcpsocketConfig(AppConfig):
    name = 'core.hosts.tcpsocket'
