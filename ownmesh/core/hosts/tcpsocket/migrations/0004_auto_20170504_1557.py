# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-04 13:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tcpsocket', '0003_auto_20170503_1609'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tcpsocketconfig',
            name='error',
        ),
        migrations.RemoveField(
            model_name='tcpsocketconfig',
            name='status',
        ),
    ]
