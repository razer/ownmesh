from __future__ import absolute_import

from core.hosts.config import HOSTTYPES, TCPSOCKET
import logging
logger = logging.getLogger(__name__)

default_app_config = 'core.hosts.tcpsocket.apps.TcpsocketConfig'

socketserver = None  # @UnusedVariable
socket = None  # @UnusedVariable

try:
    import socketserver
    import socket
except Exception:
    if TCPSOCKET in HOSTTYPES:
        logger.error('Python Socket module not found, ' +
                     'TCP Socket support is disabled')
        logger.error('Please install python-socket')
