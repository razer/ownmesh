import sys
import asyncio
from celery import task

from core.hosts.utils import tobytes
from core.hosts.pluginConnector import PluginConnector
from values.hosts import FRAME as F
from .tasks import on_new_sockrx_task

import logging
logger = logging.getLogger(__name__)


class Connector(PluginConnector):

    def begin(self):
        '''
            Initialize plugin
        '''
        super(Connector, self).begin()
        self.server = {}
        self.port = self.config.listen_port
        for addr in self.config.listen_address:
            try:
                self.server[addr] = asyncio.start_server(self.server_cb,
                                                         addr, self.port)
            except Exception:
                logger.error(f'{sys.exc_info()[:1]} from {addr}:{self.port}')

        if not self.server:
            return False

        self.server_tasks = []
        for address, server in self.server.items():
            logger.info(f'Listening on {address}:{self.port}')
            self.server_tasks.append(
                asyncio.tasks.ensure_future(server, loop=self.loop))
        return True

    async def _write(self, transport, payload):
        address, port = transport.split(':')

        try:
            _, writer = await asyncio.open_connection(address, port,
                                                      loop=self.loop)
            logger.info(f'Writing {payload} to {address}:{port}')
            writer.write(tobytes(payload) + F.NEWLINE + F.CRETURN)
            writer.close()
            return True
        except Exception:
            logger.info(f'Raised {sys.exc_info()[1]}')
            return False

    def write(self, transport, payload):
        '''
            Send to host bytes payload
        '''
        self.loop.create_task(self._write(transport, payload))

    async def _stop(self):
        for address, server in self.server.items():
            logger.info(f'Stopping {address}:{self.port}')
        for _task in self.server_tasks:
            server = _task.result()
            server.close()
            await server.wait_closed()

    def stop(self):
        self.loop.create_task(self._stop())

    def server_cb(self, reader, writer):
        self.loop.create_task(self.handler(reader, writer))

    async def handler(self, reader, writer):
        payload = await reader.readline()
        address = writer.get_extra_info('peername')[0]
        if not payload:
            logger.error(f'{address} have send an empty packet !')
            return
        logger.info(f'Received {payload} from {address}')
        on_new_sockrx_task.delay(address, payload)  # @UndefinedVariable
