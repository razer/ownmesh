from django.db import models
from django.contrib.postgres.fields import ArrayField

from core.hosts.models import Host, HostPluginConfig

import logging
logger = logging.getLogger(__name__)


class TcpsocketConfig(HostPluginConfig):
    listen_address = ArrayField(
        models.GenericIPAddressField(default='0.0.0.0'),
        default=['0.0.0.0'])
    listen_port = models.IntegerField(default=20080)


class Tcpsocket(Host):
    address = models.GenericIPAddressField(default='0.0.0.0')
    port = models.IntegerField(default=20080)

    def __str__(self):
        return self.name

    def get_parent(self):
        return Host.objects.get(id=self.id)

    def get_sendheader(self):
        return '%s:%s' % (self.address, self.port)

    def save(self, **kwargs):
        super(Tcpsocket, self).save(**kwargs)
        h = Host.objects.get(id=self.id)
        h.save(**kwargs)
