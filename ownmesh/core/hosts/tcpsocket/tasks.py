import logging
from celery import task
from core.hosts.tcpsocket.models import Tcpsocket

logger = logging.getLogger(__name__)


def _get_instance(address):
    f = Tcpsocket.objects.filter(address=address)
    if not f.count():
        logger.error(f'Tcpsock: {address} not found')
        return
    if f.count() != 1:
        logger.warning(f'Tcpsock: multiple data model with address {address}, '
                       + f'taking first')
    return f[0]


@task(queue='services')
def on_port_init_task(PORT):
    return


@task(queue='services')
def on_new_sockrx_task(address, payload):
    host = _get_instance(address)
    if not host:
        return True
    logger.info(f'New sockx, payload {payload}')
    host.get_payload(payload)
