from django.contrib import admin
from core.hosts.admin import BaseHostAdmin, HostPluginConfigAdmin
from .models import Tcpsocket, TcpsocketConfig


@admin.register(TcpsocketConfig)
class TcpsocketConfigAdmin(HostPluginConfigAdmin):
    plugin_fields = ('listen_address', 'listen_port')


@admin.register(Tcpsocket)
class TcpsocketAdmin(BaseHostAdmin):
    plugin_fields = ('address', 'port')
