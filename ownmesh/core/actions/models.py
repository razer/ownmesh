import json
import calendar
import logging

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator
from django_celery_beat.models import PeriodicTask, CrontabSchedule

from core.datas.models import Data
from values import actions as V
from values.hosts import GUICOLOR_CHOICES
import values.datas as VD

logger = logging.getLogger(__name__)


class Action(models.Model):
    description = models.TextField(default='Undescripted Action',
                                   max_length=200)
    guicolor = models.CharField(max_length=8,
                                choices=GUICOLOR_CHOICES,
                                default='gry')
    enabled = models.BooleanField(default=True)
    visible = models.BooleanField(default=True)
    run_args = models.TextField(default='', max_length=128, blank=True)

    @property
    def name(self):
        return f'{self.get_type().upper()}: {self.get_model().name}'

    @property
    def shortname(self):
        return f'{self.get_type().upper()}:{self.get_model().shortname}'

    @property
    def action_type(self):
        return self.get_type()

    @property
    def inputdata(self):
        action = self.get_model()
        if self.get_type() in 'setvalue':
            return None
        return action.inputdata.pk

    @property
    def setpointdata(self):
        if self.get_type() not in 'compare':
            return None
        return self.get_model().setpointdata.pk

    @property
    def outputdata(self):
        return self.get_model().outputdata.pk

    def __str__(self):
        return self.name

    def _get_action_attributes(self):
        attributes = {'type': None, 'model': None}
        for validType in ['setvalue', 'compare', 'sync']:
            if hasattr(self, validType):
                attributes['type'] = validType
                attributes['model'] = getattr(self, validType)
                break
        return attributes

    def get_model(self):
        return self._get_action_attributes()['model']

    def get_type(self):
        return self._get_action_attributes()['type']

    def run(self, trigger=None):
        self.get_model().run(trigger=trigger)

    def _get_args(self):
        if not self.run_args:
            return None
        try:
            return json.loads(self.run_args)
        except Exception:
            logger.error(f'Exception, run_args {self.run_args}, ' +
                         f'action {self.name}')
            return None

    def _set_action_value(self, outputdata=None, value=None, trigger=None,
                          direction=None):
        logger.info(f'{self.name}: {outputdata} set to {value}')
        if direction is None:
            direction = VD.TRANSMITTED
        run_recursive_actions = True
        if trigger:
            run_recursive_actions = trigger.run_recursive_actions
        outputdata.set_value(
            value, direction=direction, run_actions=run_recursive_actions)


class Setvalue(Action):
    outputdata = models.ForeignKey(Data, related_name='setvalueaction',
                                   on_delete=models.CASCADE)

    @property
    def name(self):
        return f'{self.description} {self.outputdata.name}'

    @property
    def shortname(self):
        return f'{self.description} {self.outputdata.shortname}'

    def __str__(self):
        return self.name

    def run(self, trigger=None):
        args = self._get_args()
        if not args:
            logger.error(f'{self} : You must provide arguments !')
            return
        if 'value' not in args:
            logger.error(f'{self} : no value !')
            return
        self._set_action_value(
            outputdata=self.outputdata, value=args['value'], trigger=trigger)


class Compare(Action):
    inputdata = models.ForeignKey(Data, related_name='compareaction_input',
                                  on_delete=models.CASCADE)
    setpointdata = models.ForeignKey(Data,
                                     related_name='compareaction_setpoint',
                                     on_delete=models.CASCADE)
    outputdata = models.ForeignKey(Data,
                                   related_name='compareaction_output',
                                   on_delete=models.CASCADE)

    @property
    def name(self):
        return (f'input {self.inputdata.name} with {self.setpointdata.name}' +
                f', output to {self.outputdata.name}')

    @property
    def shortname(self):
        return (
            f'{self.inputdata.shortname} with {self.setpointdata.shortname}' +
            f', output to {self.outputdata.shortname}')

    def __str__(self):
        return self.name

    def run(self, trigger=None):
        args = self._get_args()
        invert = False
        if args:
            invert = args['invert'] if 'invert' in args else False
        inputvalue = self.inputdata.latest_value
        if inputvalue is None:
            return
        setpointvalue = self.setpointdata.latest_value
        if setpointvalue is None:
            return
        if float(inputvalue) < float(setpointvalue):
            outputvalue = not invert
        else:
            outputvalue = invert
        self._set_action_value(outputdata=self.outputdata, value=outputvalue,
                               trigger=trigger, direction=VD.TRANSMITTED)


class Sync(Action):
    inputdata = models.ForeignKey(Data, related_name='actionsync1',
                                  on_delete=models.CASCADE)
    outputdata = models.ForeignKey(Data, related_name='actionsync2',
                                   on_delete=models.CASCADE)

    @property
    def name(self):
        return f'{self.inputdata.name} to {self.outputdata.name}'

    @property
    def shortname(self):
        return f'{self.inputdata.shortname}->{self.outputdata.shortname}'

    def __str__(self):
        return self.name

    def run(self, trigger=None):
        src_entry = self.inputdata.latest_entry
        if src_entry is None:
            return
        self._set_action_value(
            outputdata=self.outputdata, value=src_entry.value, trigger=trigger,
            direction=VD.TRANSMITTED) #src_entry.direction)


class DataTrigger(models.Model):
    action = models.ForeignKey(Action, related_name='datatrigger',
                               on_delete=models.CASCADE)
    data = models.ForeignKey(Data, related_name='actiontrigger',
                             on_delete=models.CASCADE)
    enabled = models.BooleanField(default=True)
    visible = models.BooleanField(default=True)
    run_recursive_actions = models.BooleanField(default=True)

    def __str__(self):
        return 'Data %s -> Action %s' % (self.data, self.action)


class Scheduler(models.Model):
    data = models.ForeignKey(Data, related_name='scheduler',
                             on_delete=models.CASCADE)
    default_action = models.ForeignKey(
        Action, default=None, related_name='scheduler_as_default',
        on_delete=models.CASCADE,
        help_text=_('Action runned on zone end'))
    begin_with_hour = models.IntegerField(
        default=0, validators=[MaxValueValidator(10), MinValueValidator(0)],
        help_text=_('Hour of the day scheduler begin with'))
    type = models.CharField(
        max_length=2, choices=V.ACTIONSCHEDTYPE_CHOICES, default=V.WEEK,
        help_text=_('Type of scheduler : Day, Open days / Weekend, Week'))
    enabled = models.BooleanField(default=True)
    visible = models.BooleanField(default=True)

    @property
    def name(self):
        return self.data.name

    def __str__(self):
        return self.data.name


class SchedulerEvent(models.Model):
    scheduler = models.ForeignKey(Scheduler, related_name='event',
                                  on_delete=models.CASCADE)
    action = models.ForeignKey(Action, related_name='scheduler_event',
                               on_delete=models.CASCADE)
    day_of_week = models.CharField(default='*', max_length=1)
    start_hour = models.CharField(default='*', max_length=2)
    start_minute = models.CharField(default='*', max_length=2)
    end_hour = models.CharField(default='*', max_length=2)
    end_minute = models.CharField(default='*', max_length=2)
    periodic_task_start = models.OneToOneField(PeriodicTask,
                                               blank=True, null=True,
                                               related_name='event_as_start',
                                               on_delete=models.CASCADE)
    periodic_task_end = models.OneToOneField(PeriodicTask,
                                             blank=True, null=True,
                                             related_name='event_as_end',
                                             on_delete=models.CASCADE)

    @property
    def name(self):
        event = {'day': calendar.day_name[int(self.day_of_week)-1][:3]}
        for time_string in ['start_hour', 'start_minute', 'end_hour',
                            'end_minute']:
            event[time_string] = getattr(self, time_string)
            if len(event[time_string]) < 2:
                event[time_string] = f'0{event[time_string]}'
        return (f'{event["day"]} {event["start_hour"]}:' +
                f'{event["start_minute"]} to {event["end_hour"]}:' +
                f'{event["end_minute"]}')

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        # tz_delta = localtime(datetime.now()) - localtime(datetime.utcnow())
        # tz_delta = round(tz_delta.total_seconds()/3600)

        def update_task(task, action, day_of_week, hour, minute):
            # Convert timezone to UTC
            # hour = int(hour) - tz_delta
            # day_of_week = int(day_of_week)
            # if hour < 0:
            #     hour += 24
            #     day_of_week -= 1
            # elif hour > 23:
            #     hour -= 24
            #     day_of_week += 1
            # if day_of_week < 0:
            #     day_of_week += 7
            # elif day_of_week > 6:
            #     day_of_week -= 7
            # hour = f'{hour}'
            # day_of_week = f'{day_of_week}'
            crontab, _ = CrontabSchedule.objects.get_or_create(
                minute=minute, hour=hour, day_of_week=day_of_week)
            # crontab, _ = CrontabSchedule.objects.get_or_create(
            #     minute=minute, hour=hour, day_of_week=day_of_week,
            #     timezone=pytz.timezone(TIME_ZONE))
            if task:
                logger.info('Found periodic task to delete')
                old_crontab = task.crontab
                task.delete()
                if not old_crontab.periodictask_set.count():
                    old_crontab.delete()
            return PeriodicTask.objects.create(
                name=f'{self.scheduler.name}|{self.name}|{action.name}',
                task='core.actions.tasks.run_action_task',
                crontab=crontab, args=[action.pk],
                queue='services', enabled=True)

        self.periodic_task_start = update_task(
            self.periodic_task_start, self.action, self.day_of_week,
            self.start_hour, self.start_minute)
        self.periodic_task_end = update_task(
            self.periodic_task_end, self.scheduler.default_action,
            self.day_of_week, self.end_hour, self.end_minute)
        super(SchedulerEvent, self).save()

    def delete(self, using=None, keep_parents=False):
        for task in self.periodic_task_start, self.periodic_task_end:
            if not task:
                logger.error('delete event scheduler : no task found !')
                continue
            task.enabled = False
            task.save()
            old_crontab = task.crontab
            task.delete()
            if not old_crontab.periodictask_set.count():
                old_crontab.delete()
        super(SchedulerEvent, self).delete(using, keep_parents)

    def __str__(self):              # __unicode__ on Python 2
        return f'({self.scheduler.name}) {self.name}: {self.action.shortname}'
