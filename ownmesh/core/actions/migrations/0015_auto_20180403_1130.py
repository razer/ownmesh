# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-04-03 09:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('datas', '0009_data_on_alarm'),
        ('actions', '0014_auto_20171014_0741'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scheduler',
            name='name',
        ),
        migrations.AddField(
            model_name='scheduler',
            name='data',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='scheduler', to='datas.Data'),
        ),
    ]
