# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-04-03 09:36
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('actions', '0015_auto_20180403_1130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scheduler',
            name='data',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='scheduler', to='datas.Data'),
        ),
    ]
