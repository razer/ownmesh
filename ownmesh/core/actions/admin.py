import logging
from django.contrib import admin
from project.admin import RulesAdmin
from .models import (Action, Compare, Setvalue, Sync, DataTrigger,
                     Scheduler, SchedulerEvent)

logger = logging.getLogger(__name__)


@admin.register(Action)
class ActionAdmin(RulesAdmin):
    def has_add_permission(self, request):
        return False


@admin.register(SchedulerEvent)
class SchedulerEventAdmin(admin.ModelAdmin):
    fields = ('scheduler', 'action', 'day_of_week', 'start_hour',
              'start_minute', 'end_hour', 'end_minute')


admin.site.register(Compare)
admin.site.register(Setvalue)
admin.site.register(Sync)
admin.site.register(DataTrigger)
admin.site.register(Scheduler)
