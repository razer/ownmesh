import logging
from celery import task
from django.core.exceptions import ObjectDoesNotExist
from core.actions.models import Action

logger = logging.getLogger(__name__)


@task(queue='services')
def run_action_task(pk):
    try:
        action = Action.objects.get(pk=pk)
    except ObjectDoesNotExist:
        logger.error(f'No action : pk ${pk}')
        return True
    action.run()
