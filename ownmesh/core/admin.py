from django.contrib import admin
from .models import Config


class ConfigAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        if self.model.objects.count():
            return False
        else:
            super().has_add_permission(request)


admin.site.register(Config, admin_class=ConfigAdmin)
