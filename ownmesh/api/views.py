import logging

from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from core.hosts.models import Host
from .serializers import (HostSerializer)

# Logger
logger = logging.getLogger(__name__)


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 1000


class HostViewSet(ModelViewSet):
    queryset = Host.objects.all()
    serializer_class = HostSerializer
    pagination_class = LargeResultsSetPagination
