import logging
from rest_framework.serializers import (Serializer, ModelSerializer,
                                        SerializerMethodField)
from core.hosts.models import Host
from core.datas.models import Data
from core.actions.models import Action, Scheduler, SchedulerEvent, DataTrigger
from frontend.controls.models import Control
from frontend.events.models import EventCache
from frontend.charts.models import Chart
from values.colors import guicolor

logger = logging.getLogger(__name__)


class DataSerializer(ModelSerializer):

    class Meta:
        model = Data
        fields = (
            'id', 'name', 'shortname', 'host', 'pending', 'value_min',
            'value_max', 'value_step', 'role', 'unit', 'mode', 'entry_type',
            'entry_creation', 'alarm_data', 'on_alarm', 'always_save_entry',
            'enabled', 'visible')


class HostSerializer(ModelSerializer):
    type = SerializerMethodField()
    color = SerializerMethodField()

    class Meta:
        model = Host
        fields = (
            'id', 'name', 'color', 'type', 'netstatus_data', 'power_mode')

    def get_netstatus(self, instance):
        return instance.online

    def get_color(self, instance):
        return guicolor[instance.guicolor]

    def get_type(self, instance):
        return instance.get_type()


class EntrySerializer(Serializer):

    def to_representation(self, instance):
        """
            Add related data pk to data as data_pk
        """
        entry = super(EntrySerializer, self).to_representation(instance)
        pub_date = instance.unix_pubdate
        entry.update({'id': instance.id, 'data_pk': instance.data.pk,
                      'value': instance.value, 'pub_date': pub_date})
        return entry


class ControlSerializer(ModelSerializer):
    data = DataSerializer()

    class Meta:
        model = Control
        fields = ('id', 'data', 'position')


class ChartSerializer(ModelSerializer):
    class Meta:
        model = Chart
        fields = ('id', 'data', 'position', 'height')


class EventCacheSerializer(ModelSerializer):
    class Meta:
        fields = '__all__'
        model = EventCache


class ActionSerializer(ModelSerializer):
    color = SerializerMethodField()

    class Meta:
        model = Action
        fields = ('id', 'shortname', 'name', 'action_type', 'color', 'enabled',
                  'inputdata', 'setpointdata', 'outputdata', 'run_args')

    def get_color(self, instance):
        return guicolor[instance.guicolor]


class DataTriggerSerializer(ModelSerializer):

    class Meta:
        model = DataTrigger
        fields = ('id', 'action', 'data', 'enabled', 'visible',
                  'run_recursive_actions')


class SchedulerSerializer(ModelSerializer):

    class Meta:
        fields = (
            'id', 'data', 'name', 'default_action', 'begin_with_hour', 'type')
        model = Scheduler


class SchedulerEventSerializer(ModelSerializer):

    class Meta:
        fields = ('id', 'scheduler', 'action', 'day_of_week', 'start_hour',
                  'start_minute', 'end_hour', 'end_minute')
        model = SchedulerEvent
