import logging
from django.db import models
from core.datas.models import Data

logger = logging.getLogger(__name__)


class Control(models.Model):
    data = models.ForeignKey(Data, related_name='control',
                             on_delete=models.CASCADE)
    position = models.IntegerField(default=0)
    template = models.BooleanField(default=False)
    enabled = models.BooleanField(default=True)
    visible = models.BooleanField(default=True)

    class Meta:
        ordering = ['position']

    def __str__(self):
        return f'Control for {self.data}'

    def get_childdatas(self):
        childlist = []
        for child in self.child.all():
            if child.data.enabled and child.data.visible:
                childlist.append(child.data)
        return childlist


class Child(models.Model):
    control = models.ForeignKey(Control, related_name='child',
                                on_delete=models.CASCADE)
    data = models.ForeignKey(Data, related_name='controlchild',
                             on_delete=models.CASCADE)

    def __str__(self):
        return f'Child {self.data} from {self.control}'
