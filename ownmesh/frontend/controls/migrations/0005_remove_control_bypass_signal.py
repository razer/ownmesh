# Generated by Django 2.0.4 on 2018-05-01 18:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controls', '0004_control_bypass_signal'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='control',
            name='bypass_signal',
        ),
    ]
