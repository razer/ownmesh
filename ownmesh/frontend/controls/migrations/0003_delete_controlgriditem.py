# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-20 07:13
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    pass

    dependencies = [
        ('controls', '0002_auto_20170720_0913'),
        # ('grids', '0002_auto_20170720_0913'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ControlGridItem',
        ),
    ]
