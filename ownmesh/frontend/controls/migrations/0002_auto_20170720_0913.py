# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-20 07:13
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controls', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='controlgriditem',
            name='control',
        ),
        migrations.RemoveField(
            model_name='controlgriditem',
            name='griditem_ptr',
        ),
        migrations.AlterModelOptions(
            name='control',
            options={'ordering': ['position']},
        ),
        migrations.RenameField(
            model_name='control',
            old_name='row',
            new_name='position',
        ),
    ]
