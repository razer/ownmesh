import logging
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete

from api.serializers import ControlSerializer, DataSerializer, EntrySerializer
from frontend.controls.models import Control, Child
from frontend.consumers import sse_push

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Control)
def on_control_saved(sender, **kwargs):
    control = kwargs['instance']
    logger.debug(f'post_saved signal: {control}')
    sse_push('control', ControlSerializer(control).data)
    sse_push('data', DataSerializer(control.data).data)
    latest_entry = control.data.latest_entry
    if latest_entry:
        sse_push('lastentry', EntrySerializer(latest_entry).data)


@receiver(post_delete, sender=Control)
def on_control_deleted(sender, **kwargs):
    control = kwargs['instance']
    logger.debug(f'post_delete signal: {control}')
    sse_push('control', {'id': control.id, 'deleted': True})


@receiver(post_save, sender=Child)
@receiver(post_delete, sender=Child)
def on_child_saved(sender, **kwargs):
    child = kwargs['instance']
    logger.debug(f'post_saved/deleted signal for control child {child.id}')
    childdata_map = []
    for child in child.control.child.all():
        sse_push('data', DataSerializer(child.data).data)
        latest_entry = child.data.latest_entry
        if latest_entry:
            sse_push('lastentry', EntrySerializer(latest_entry).data)
        childdata_map.append(child.data)
    sse_push('childdata', {
        'id': child.control.pk,
        'childdata_map': DataSerializer(childdata_map, many=True).data})
