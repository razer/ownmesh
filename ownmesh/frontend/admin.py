from django.contrib import admin

# Register your models here.
from frontend.controls.models import Control, Child
from frontend.events.models import EventConfig, EventCache
from frontend.charts.models import Chart

admin.site.register(Control)
admin.site.register(Child)
admin.site.register(EventConfig)
admin.site.register(EventCache)
admin.site.register(Chart)
