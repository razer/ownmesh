import logging
from django.db import models
from core.datas.models import Data
from values.charts import MEDIUM, CHARTHEIGHT_CHOICES

logger = logging.getLogger(__name__)


class Chart(models.Model):
    data = models.OneToOneField(Data, related_name='chart',
                                on_delete=models.CASCADE)
    position = models.IntegerField(default=0)
    height = models.IntegerField(choices=CHARTHEIGHT_CHOICES,
                                 default=MEDIUM)
    enabled = models.BooleanField(default=True)
    visible = models.BooleanField(default=True)

    class Meta:
        ordering = ['position']

    def __str__(self):
        return f'Chart for {self.data.shortname}'
