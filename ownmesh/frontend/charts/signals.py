import logging
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete

from api.serializers import ChartSerializer
from frontend.consumers import sse_push
from .models import Chart

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Chart)
def on_child_saved(sender, **kwargs):
    chart = kwargs['instance']
    logger.info(f'Chart post_saved signal: {chart}')
    sse_push('chart', ChartSerializer(chart).data)


@receiver(post_delete, sender=Chart)
def on_child_deleted(sender, **kwargs):
    chart = kwargs['instance']
    logger.info(f'Chart post_delete signal: {chart}')
    sse_push('chart', {'id': chart.id, 'deleted': True})
