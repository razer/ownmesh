from django.test import TestCase
from frontend.consumers import ConsumerPayload, ConsumerDispatcher
import json


class ConsumerTestCase(TestCase):

    def test_payloader_dispatcher(self):
        print('hello')
        message = {}
        message['data'] = ''
        message['id'] = '1458'
        message['type'] = 'request'
        message['name'] = 'controls_init'

        payload = json.dumps(message)
        cp = ConsumerPayload(payload)
        self.assertIsNotNone(cp.decode(),
                             msg='Decode() -> none')

        cd = ConsumerDispatcher(cp)
        response = cd.dispatch()
        self.assertIsNotNone(response,
                             msg='Dispatch() -> none')

        cp2 = ConsumerPayload(response)
        self.assertIsNotNone(cp2.encode(),
                             msg='Encode() -> none')
