from django.contrib.sessions.models import Session
from celery import task
from .consumers import sse_push


@task(queue='services')
def sse_push_task(*args, **kwargs):
    sse_push(*args, **kwargs)


@task(queue='services')
def session_cleanup_task():
    Session.objects.all().delete()
