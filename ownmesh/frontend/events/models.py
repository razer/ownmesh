import logging
from django.db import models
from django.utils import timezone
from values.events import (
    EVENTTYPE_CHOICES, ALL, SIZE_DEFAULT, DEFAULTTYPE_CHOICES, LAST
    )

logger = logging.getLogger(__name__)


class EventConfig(models.Model):
    max_items = models.IntegerField(default=SIZE_DEFAULT)
    last_type = models.CharField(max_length=2,
                                 choices=EVENTTYPE_CHOICES,
                                 default=ALL)
    default_type = models.CharField(max_length=2,
                                    choices=DEFAULTTYPE_CHOICES,
                                    default=LAST)


class EventCache(models.Model):
    event_type = models.CharField(max_length=2,
                                  choices=EVENTTYPE_CHOICES,
                                  default=ALL)
    content = models.TextField(default='', max_length=1000)
    pub_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.event_type
