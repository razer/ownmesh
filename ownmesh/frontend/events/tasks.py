import logging
import json
from celery import task
from django.utils import timezone
from django.apps import apps as djangoapps
from api.serializers import EventCacheSerializer
from frontend.consumers import sse_push
from values.events import ALARM, ALL, NETSTATUS, DATAVALUE
from values.colors import guicolor
from values.datas import TRANSMITTED
from .models import EventCache, EventConfig

logger = logging.getLogger(__name__)

hosttype = {ALL: None, ALARM: 'alarm', DATAVALUE: None, NETSTATUS: 'netstatus'}


# def _sse_push(event_type, content):
#     content.insert(0, {'event_type': event_type})
#     sse_push('eventlist', content)


@task(queue='services')
def purge_eventcache_task(event_type=None):
    if not event_type:
        for ec in EventCache.objects.all():
            ec.delete()
    else:
        if EventCache.objects.filter(event_type=event_type).count():
            EventCache.objects.get(event_type=event_type).delete()


@task(queue='services')
def refresh_eventcache_task(entry_type, pkentry):  # pylint: disable=R0914
    entry_type = f'{entry_type.lower().capitalize()}Entry'
    entry_model = djangoapps.get_model(app_label='datas',
                                       model_name=entry_type)
    eventcache_map = [ALL]
    entry = entry_model.objects.get(pk=pkentry)
    datamodel = entry.data.get_type()
    event = {'date': entry.human_pubdate, 'color': 'red'}
    if not datamodel:
        eventcache_map.append(DATAVALUE)
        event['color'] = guicolor[entry.data.host.guicolor]
        direction = 'received'
        if entry.direction == TRANSMITTED:
            direction = 'transmitted'
        value = entry.value
        if isinstance(entry.value, bool):
            value = int(entry.value)
        event['message'] = f'{entry.data.shortname}: {direction} value {value}'
        if entry.data.unit:
            event['message'] += entry.data.unit
    elif 'netstatus' in datamodel:
        eventcache_map.append(NETSTATUS)
        host = entry.data.netstatus.related_host
        event['message'] = f'{host.name} is now offline'
        if entry.value:
            event['color'] = guicolor[host.guicolor]
            event['message'] = event['message'].replace('offline', 'online')
    elif 'alarm' in datamodel:
        eventcache_map.append(ALARM)
        data = entry.data.alarm.related_data
        event['message'] = f'Alarm for {data.name}: {data.latest_value}'
        if data.unit:
            event['message'] += data.unit
        if not entry.value:
            event['color'] = guicolor[data.host.guicolor]
            event['message'] = f'End of {event["message"]}'
    else:
        return
    event_conf, _ = EventConfig.objects.get_or_create()
    for event_type in eventcache_map:
        ev, created = EventCache.objects.get_or_create(event_type=event_type)
        content = [] if created else json.loads(ev.content)
        content_size = len(content)
        if content_size >= event_conf.max_items:
            content.pop(content_size - 1)
        content.insert(0, event)
        ev.content = json.dumps(content)
        ev.pub_date = timezone.now()
        ev.save()
        sse_push(name='event', data=EventCacheSerializer(ev).data)
