import sys
import json
import logging
from random import random
from datetime import datetime
from collections import OrderedDict

from asgiref.sync import async_to_sync #, sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from channels.layers import get_channel_layer
from channels.auth import get_user

from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from rest_framework.metadata import SimpleMetadata
from core.hosts.models import Host
from core.datas.models import Data
from core.actions.models import Action, Scheduler, DataTrigger
from frontend.controls.models import Control
from frontend.events.models import EventCache
from frontend.charts.models import Chart
from api.serializers import (
    ControlSerializer, HostSerializer, EntrySerializer, DataSerializer,
    EventCacheSerializer, SchedulerSerializer, SchedulerEventSerializer,
    ChartSerializer, ActionSerializer, DataTriggerSerializer)
from values.datas import NOPERSISTANCE, STRING

logger = logging.getLogger(__name__)


class ConsumerJson:

    def __init__(self, payload):
        self.payload = payload
        self.items = ('genre', 'name', 'many', 'id', 'data')

    def decode(self):
        if not self.payload:
            logger.error('No payload !')
            return None
        try:
            self.payload = json.loads(self.payload)
        except Exception:
            logger.error('Bad payload : json raise error')
            return None
        for item in self.items:
            if 'many' in item:
                continue
            if item not in self.payload.keys():
                logger.error('Bad payload')
                return None
            self.__dict__[item] = self.payload[item]
        return self

    def encode(self):
        if not self.payload:
            logger.error('No payload !')
            return None
        result = {}
        for item in self.items:
            if not hasattr(self.payload, item):
                logger.error(f'Bad payload : {item} missing')
                return None
            result[item] = getattr(self.payload, item)
        try:
            return json.dumps(result)
        except Exception:
            logger.error('Bad payload : json raise error')
            logger.error(result)
            return None


class Response:
    # pylint: disable=W0622,R0913
    def __init__(self, id=int(random() * 10000), genre='response', name=None,
                 many=False, data=None):
        self.id = id
        self.genre = genre
        self.name = name
        self.many = many
        self.data = data


class FakeData:
    def __init__(self, shortname='Unknown data', pk=None):
        self.pk = pk
        self.shortname = shortname


class BaseConsumer(AsyncWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super(BaseConsumer, self).__init__(*args, **kwargs)
        self.response = None
        self.payload = None
        self.options = None
        self.default_request_options = {'already_fetched': ''}
        self.user = None

    async def connect(self):
        @database_sync_to_async
        def db_session_request(session_key):
            if not Session.objects.filter(session_key=session_key).count():
                return None
            return Session.objects.get(session_key=session_key)

        @database_sync_to_async
        def db_user_request(user_id):
            return User.objects.get(pk=user_id)

        self.user = await get_user(self.scope)
        if self.user.is_anonymous:
            session_key = self.scope['query_string'].decode('utf-8')
            session = await db_session_request(session_key)
            if not session:
                logger.error(f'No session found for key --{session_key}--')
                await self.close()
                return
            user_id = session.get_decoded().get('_auth_user_id')
            self.user = await db_user_request(user_id)
            self.scope['user'] = self.user
        logger.info(f'New socket for {self.user.username}')
        await self.accept()
        await self.channel_layer.group_add('ownmesh', self.channel_name)

    # pylint: disable=E1101
    async def receive(self, text_data=None, bytes_data=None):
        # logger.info(f'Consumer receive: {text_data}')
        self.payload = ConsumerJson(text_data).decode()
        if not self.payload:
            return
        try:
            self.options = json.loads(self.payload.data)
        except Exception:
            self.options = self.default_request_options
        self.response = Response(id=self.payload.id)
        try:
            await getattr(self, f'{self.payload.name}_{self.payload.genre}')()
        except Exception:
            logger.error(f'Calling {self.payload.name}_{self.payload.genre} ' +
                         f'raise {sys.exc_info()[1]}')

    async def server_push(self, event):
        await self.send(text_data=event['text'])

    async def disconnect(self, code):
        logger.info(f'Socket disconnected ({code}) for {self.user.username}')

    @database_sync_to_async
    def serialized(self, serializer, data, many=False):
        return serializer(data, many=many).data

    async def scheduler_map_request(self):

        @database_sync_to_async
        def db_request():
            result = Scheduler.objects.all()
            if result.count():
                return result
            return None

        if 'action' not in self.options['already_fetched']:
            await self.action_map_request()
        self.response.name = 'scheduler'
        self.response.many = True
        scheduler_map = await db_request()
        self.response.data = await self.serialized(
            SchedulerSerializer, scheduler_map, many=True)
        if not self.response.data:
            return
        await self.send(text_data=ConsumerJson(self.response).encode())
        self.response.name = 'scheduler_metadata'
        self.response.many = False
        self.response.data = SimpleMetadata().get_serializer_info(
            SchedulerSerializer())
        if not self.response.data:
            return scheduler_map
        await self.send(text_data=ConsumerJson(self.response).encode())
        return scheduler_map

    # pylint: disable=E1101, W0201
    async def scheduler_event_map_request(self, scheduler=None):

        @database_sync_to_async
        def db_request():
            result = Scheduler.objects.filter(pk=self.payload.data)
            if result.count():
                return result.first()
            return None

        @database_sync_to_async
        def db_event(scheduler):
            if scheduler.event.count():
                return list(scheduler.event.all())
            return None

        scheduler = scheduler or await db_request()
        if not scheduler:
            self.payload.data = {'id': 'error', 'scheduler': self.payload.data,
                                 'message': 'ObjectDoesNotExist'}
            await self.send(text_data=ConsumerJson(self.payload).encode())
            return

        async def send_events():
            if self.response.data is None:
                self.response.data = []
            self.response.data.append(OrderedDict([
                ('id', 'endframe'), ('scheduler', scheduler.pk)]))
            # logger.info(f'Send entries : {data.shortname} - {start}->{end}')
            await self.send(text_data=ConsumerJson(self.response).encode())

        self.response.name = 'scheduler_event'
        event_map = await db_event(scheduler)
        if not event_map:
            self.response.many = False
            self.response.data = {'id': 'empty', 'scheduler': scheduler.pk}
            await self.send(text_data=ConsumerJson(self.response).encode())
            return
        self.response.many = True
        self.response.data = await self.serialized(
            SchedulerEventSerializer, event_map, many=True)
        await send_events()

    async def host_netstatus_map_request(self, host_map=None):

        @database_sync_to_async
        def db_request():
            return Host.objects.all()

        if not host_map:
            logger.info('No host map, db request...')
            host_map = await db_request()
        for host in host_map:
            if not hasattr(host, 'netstatus_data'):
                continue
            await self.data_request(data=host.netstatus_data)
            await self.lastentry_request(host.netstatus_data)

    async def host_map_request(self):

        @database_sync_to_async
        def db_request():
            return Host.objects.all()

        host_map = await db_request()
        self.response.name = 'host'
        self.response.many = True
        self.response.data = await self.serialized(
            HostSerializer, host_map, many=True)
        await self.send(text_data=ConsumerJson(self.response).encode())
        return host_map

    async def lastentry_request(self, data=None):

        @database_sync_to_async
        def db_request(data):
            if data is None:
                return Data.objects.get(pk=self.payload.data).latest_entry
            return data.latest_entry

        latest_entry = await db_request(data)
        self.response.name = 'lastentry'
        self.response.many = False
        if not latest_entry:
            data_pk = self.payload.data if data is None else data.pk
            self.response.data = {'id': 'empty', 'data_pk': data_pk}
            await self.send(text_data=ConsumerJson(self.response).encode())
            return
        self.response.data = await self.serialized(
            EntrySerializer, latest_entry)
        await self.send(text_data=ConsumerJson(self.response).encode())

    async def alarm_data_request(self, data, with_entry=False):
        @database_sync_to_async
        def db_count_request():
            return data.alarm_data.count()

        @database_sync_to_async
        def db_request():
            return data.alarm_data.first()

        if not await db_count_request():
            return
        alarm_data = await db_request()
        self.response.name = 'data'
        self.response.many = False
        self.response.data = await self.serialized(DataSerializer, alarm_data)
        await self.send(text_data=ConsumerJson(self.response).encode())
        if not with_entry:
            return
        await self.lastentry_request(alarm_data)

    # pylint: disable=E1101
    async def data_request(self, data=None, many=False):

        @database_sync_to_async
        def db_request():
            return Data.objects.get(pk=self.payload.data)

        if not data:
            data = await db_request()
        self.response.name = 'data'
        self.response.many = many
        self.response.data = await self.serialized(
            DataSerializer, data, many=many)
        await self.send(text_data=ConsumerJson(self.response).encode())

    async def data_map_request(self):

        @database_sync_to_async
        def db_request():
            return list(Data.objects.all())

        if not self.options:
            return
        if 'host' not in self.options['already_fetched']:
            await self.host_map_request()
        data_map = await db_request()
        logger.info(f'Request for data map: got data_map')
        await self.data_request(data_map, many=True)
        self.response.name = 'data_map_state'
        self.response.data = {'id': 'endframe', 'pk': 0}
        await self.send(text_data=ConsumerJson(self.response).encode())
        return data_map

    async def data_landing_request(self):

        @database_sync_to_async
        def db_request():
            return list(Data.objects.all())

        if not self.options:
            return
        logger.info(f'Request for data landing')
        if 'data' not in self.options['already_fetched']:
            data_map = await self.data_map_request()
        else:
            data_map = await db_request()
        logger.info(f'Request for data landing: got data_map')
        for data in data_map:
            await self.lastentry_request(data)
        self.response.name = 'data_landing_state'
        self.response.data = {'id': 'endframe', 'pk': 0}
        await self.send(text_data=ConsumerJson(self.response).encode())

    async def event_map_request(self):

        @database_sync_to_async
        def db_request():
            return EventCache.objects.all()

        self.response.name = 'event'
        self.response.many = True
        self.response.data = await self.serialized(
            EventCacheSerializer, await db_request(), many=True)
        await self.send(text_data=ConsumerJson(self.response).encode())
        self.response.name = 'event_landing_state'
        self.response.data = {'id': 'endframe', 'pk': 0}
        await self.send(text_data=ConsumerJson(self.response).encode())

    async def control_childdata_request(self, control):

        @database_sync_to_async
        def db_request(control):
            data_list = []
            for child in control.child.all():
                data_list.append(child.data)
            return data_list

        self.response.name = 'childdata'
        data_map = await db_request(control)
        self.response.data = {
            'id': control.pk,
            'childdata_map': await self.serialized(
                DataSerializer, data_map, many=True)}
        await self.send(text_data=ConsumerJson(self.response).encode())
        return data_map

    async def control_landing_request(self):
        @database_sync_to_async
        def db_request():
            return Control.objects.all()
            # return Control.objects.select_related('data').all()

        @database_sync_to_async
        def db_data_request(pk):
            return Data.objects.get(pk=pk)

        @database_sync_to_async
        def db_childdata_request(control):
            return control.get_childdatas()

        # Step 1 : get controls & associated main datas
        control_map = await db_request()
        self.response.name = 'control'
        self.response.many = True
        self.response.data = await self.serialized(
            ControlSerializer, control_map, many=True)
        # self.response.data = ControlSerializer(control_map, many=True).data
        await self.send(text_data=ConsumerJson(self.response).encode())
        # Step 2 : get hosts
        host_map = None
        if 'host' not in self.options['already_fetched']:
            host_map = await self.host_map_request()
        # Step 3 : get main last entries
        if 'data' not in self.options['already_fetched']:
            for control in control_map:
                await self.lastentry_request(control.data)
            # Step 4 : get host netstatus data
            await self.host_netstatus_map_request(host_map)
        # Step 5 : get childs datas
        child_data_list = []
        for control in control_map:
            data_map = await self.control_childdata_request(control)
            for child_data in data_map:
                if child_data.pk not in child_data_list:
                    child_data_list.append(child_data.pk)
        # Step 6 : get childs datas last entries
        for child_data_pk in child_data_list:
            await self.lastentry_request(await db_data_request(child_data_pk))

        if 'event' not in self.options['already_fetched']:
            # Step 7 : get events
            await self.event_map_request()

        if 'data' not in self.options['already_fetched']:
            # Step 8 : Send Alarm datas
            for control in control_map:
                await self.alarm_data_request(control.data, with_entry=True)
                for child_data in await db_childdata_request(control):
                    await self.alarm_data_request(child_data, with_entry=True)
        self.response.name = 'control_landing_state'
        self.response.data = {'id': 'endframe', 'pk': 0}
        await self.send(text_data=ConsumerJson(self.response).encode())

    async def data_entries_request(self):

        @database_sync_to_async
        def db_data_get(pk):
            result = Data.objects.filter(pk=pk)
            if result.count():
                return result.first()
            return None

        @database_sync_to_async
        def db_entries_filter(data, start, end):
            result = data.entries.filter(pub_date__range=[start, end])
            if result.count():
                return list(result)
            return None

        @database_sync_to_async
        def db_netstatus_data(data):
            if hasattr(data.host, 'netstatus_data'):
                return data.host.netstatus_data
            return None

        @database_sync_to_async
        def db_alarmdata(data):
            if hasattr(data, 'alarmdata') and data.alarmdata.count():
                return data.alarmdata.first()
            return None

        async def on_error(data, err_msg='Unknown error'):
            logger.warning(f'data_entries for {data.shortname}: {err_msg}')
            self.response.data = OrderedDict([
                ('id', 'error'), ('data_pk', data.pk), ('message', err_msg)])
            self.response.many = False
            await self.send(text_data=ConsumerJson(self.response).encode())

        async def send_entries():
            if self.response.data is None:
                self.response.data = []
            self.response.data.append(OrderedDict([
                ('id', 'endframe'), ('data_pk', data_pk),
                ('start', payload_data['start'])]))
            logger.info(f'Send entries : {data.shortname} - {start}->{end}')
            await self.send(text_data=ConsumerJson(self.response).encode())

        payload_data = json.loads(self.payload.data)  # pylint: disable=E1101
        data_pk = payload_data['data_pk']
        start = datetime.fromtimestamp(payload_data['start']/1000)
        end = datetime.fromtimestamp(payload_data['end']/1000)
        self.response.many = True
        self.response.name = 'entry'
        data = await db_data_get(data_pk)
        if not data:
            await on_error(FakeData(pk=data_pk), 'Data not found')
            return
        if NOPERSISTANCE in data.entry_creation:
            await on_error(data, 'Data not persistent')
            return
        if STRING in data.entry_type:
            await on_error(data, 'Data is string')
            return
        # Main data entries
        entries = await db_entries_filter(data, start, end)
        if not entries:
            await send_entries()
            return
        self.response.data = await self.serialized(
            EntrySerializer, entries, many=True)
        # Host netstatus_data entries
        netstatus_data = await db_netstatus_data(data)
        if netstatus_data:
            entries = await db_entries_filter(netstatus_data, start, end)
            if entries:
                self.response.data += await self.serialized(
                    EntrySerializer, entries, many=True)
        # Alarm data entries
        alarm_data = await db_alarmdata(data)
        if alarm_data:
            entries = await db_entries_filter(alarm_data, start, end)
            if entries:
                self.response.data += await self.serialized(
                    EntrySerializer, entries, many=True)
        await send_entries()

    async def chart_landing_request(self):

        @database_sync_to_async
        def db_request():
            return Chart.objects.filter(enabled=True, visible=True)

        # Step 1 : get charts
        chart_map = await db_request()
        self.response.name = 'chart'
        self.response.many = True
        self.response.data = await self.serialized(
            ChartSerializer, chart_map, many=True)
        await self.send(text_data=ConsumerJson(self.response).encode())
        # Step 2 : get hosts
        if 'host' not in self.options['already_fetched']:
            await self.host_map_request()
        self.response.name = 'chart_landing_state'
        self.response.data = {'id': 'endframe', 'pk': 0}
        await self.send(text_data=ConsumerJson(self.response).encode())

    async def datatrigger_map_request(self):

        @database_sync_to_async
        def db_request():
            return DataTrigger.objects.all()

        self.response.name = 'datatrigger'
        self.response.many = True
        datatrigger_map = await db_request()
        self.response.data = await self.serialized(
            DataTriggerSerializer, datatrigger_map, many=True)
        await self.send(text_data=ConsumerJson(self.response).encode())
        return datatrigger_map

    async def action_map_request(self, landing=False):

        @database_sync_to_async
        def db_request():
            return Action.objects.all()

        @database_sync_to_async
        def data_db_request(pk):
            return Data.objects.get(pk=pk)

        # Step 1 : actions
        action_map = await db_request()
        self.response.name = 'action'
        self.response.many = True
        self.response.data = await self.serialized(
            ActionSerializer, action_map, many=True)
        await self.send(text_data=ConsumerJson(self.response).encode())
        # Step 2 : related data & datatriggers
        if landing:
            datatrigger_map = await self.datatrigger_map_request()
        if 'data' not in self.options['already_fetched']:
            datapk_map = []
            # data_map = []
            for action in action_map:
                if action.outputdata not in datapk_map:
                    await self.data_request(
                        await data_db_request(action.outputdata))
                    datapk_map.append(action.outputdata)
                if not landing:
                    continue
                for data_name in 'inputdata', 'setpointdata':
                    data_pk = getattr(action, data_name)
                    if data_pk is None or data_pk in datapk_map:
                        continue
                    await self.data_request(await data_db_request(data_pk))
                    datapk_map.append(data_pk)
                if datatrigger_map:
                    for datatrigger in datatrigger_map:
                        if datatrigger.data.pk in datapk_map:
                            continue
                        await self.data_request(datatrigger.data)
                        datapk_map.append(datatrigger.data.pk)

        self.response.name = 'action'
        self.response.many = False
        self.response.data = {'id': 'endframe', 'pk': 0}
        await self.send(text_data=ConsumerJson(self.response).encode())
        if not landing:
            return
        scheduler_map = await self.scheduler_map_request()
        if not scheduler_map:
            return
        for scheduler in scheduler_map:
            await self.scheduler_event_map_request(scheduler)

    async def action_landing_request(self):
        await self.action_map_request(landing=True)
        self.response.name = 'action_landing_state'
        self.response.many = False
        self.response.data = {'id': 'endframe', 'pk': 0}
        await self.send(text_data=ConsumerJson(self.response).encode())


def sse_push(name=None, data=None, many=False):
    content = {'genre': 'push', 'id': int(random() * 10000),
               'name': name, 'many': many, 'data': data}
    try:
        async_to_sync(get_channel_layer().group_send)(
            'ownmesh', {'type': 'server.push', 'text': json.dumps(content)})
    except PermissionError:
        pass
