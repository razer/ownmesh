from django.conf.urls import url
from frontend import views

urlpatterns = [
    url(r'^api/test/$',
        views.test,
        name='test'),

    url(r'^api/entry/$',
        views.entry,
        name='entry'),

    url(r'^api/control/positions/$',
        views.control_positions,
        name='control_positions'),

    url(r'^api/control/save/$',
        views.control_save,
        name='control_save'),

    url(r'^api/control/delete/$',
        views.control_delete,
        name='control_delete'),

    url(r'^api/chart/positions/$',
        views.chart_positions,
        name='chart_positions'),

    url(r'^api/chart/add/$',
        views.chart_add,
        name='chart_add'),

    url(r'^api/chart/remove/$',
        views.chart_remove,
        name='chart_remove'),

    url(r'^api/chart/height/$',
        views.chart_height,
        name='chart_height'),

    url(r'^api/scheduler/config/$',
        views.scheduler_config,
        name='scheduler_config'),

    url(r'^api/scheduler/event/$',
        views.scheduler_event,
        name='scheduler_event'),

    url(r'^api/auth/csrftoken/$',
        views.csrftoken,
        name='csrftoken'),

    url(r'^api/auth/login/$',
        views.login,
        name='auth_login'),

    url(r'^api/auth/logout/$',
        views.logout,
        name='auth_logout'),
]
