import logging
from django.apps import AppConfig

logger = logging.getLogger(__name__)


class FrontendConfig(AppConfig):
    name = 'frontend'

    # flake8: noqa
    # pylint: disable=W0612
    def ready(self):
        logger.debug('Frontend app signals init')
        from frontend.controls import signals as ctlsignals
        from frontend.charts import signals as chtsignals
