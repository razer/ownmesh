import json
import logging

from django.middleware import csrf
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.http import require_POST
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import (authenticate, login as django_login,
                                 logout as django_logout)

from core.datas.models import Data
from core.actions.models import Scheduler, SchedulerEvent, Action
from .controls.models import Control, Child
from .charts.models import Chart

logger = logging.getLogger(__name__)


def login_required_ajax(function=None, redirect_field_name=None):
    """
    Just make sure the user is authenticated to access a certain ajax view

    Otherwise return a HttpResponse 401 - authentication required
    instead of the 302 redirect of the original Django decorator
    """
    def _decorator(view_func):
        def _wrapped_view(request, *args, **kwargs):
            if request.user.is_authenticated:
                return view_func(request, *args, **kwargs)
            return JsonResponse({'text': _('Unauthorized')}, status=401)
        return _wrapped_view

    if function is None:
        return _decorator
    return _decorator(function)


def check_post_request(fields):
    """
    Check for fields presence in request
    """
    def decorator(func):
        def inner(request, *args, **kwargs):
            logger.debug(f'Checking request: {request.POST}')
            for field in fields:
                if field not in request.POST.keys():
                    return JsonResponse({'text': f'Missing field {field}'},
                                        status=400)
            return func(request, *args, **kwargs)
        return inner
    return decorator


def csrftoken(request):
    try:
        token = csrf.get_token(request)
    except Exception as error:
        text = f'{_("Get token failed")} : {error}'
        logger.error(text)
        return JsonResponse({'text': text}, status=400)
    if not token:
        text = _('Empty token')
        logger.error(text)
        return JsonResponse({'text': text}, status=400)
    return JsonResponse({'text': token}, status=200)


@require_POST
@check_post_request(['username', 'password'])
def login(request):
    user = authenticate(request, username=request.POST['username'],
                        password=request.POST['password'])
    if not user:
        text = f'{_("Auth failed for")} {request.POST["username"]}'
        logger.error(text)
        return JsonResponse({'text': text}, status=401)

    django_login(request, user)
    logger.info(f'{user.username} logged, key {request.session.session_key}')
    return JsonResponse({'text': request.session.session_key}, status=200)


def logout(request):
    user = request.user.username
    session_key = request.session.session_key
    logger.info(f'Logging out for {user} with key {session_key}')
    django_logout(request)
    if request.session.exists(session_key):
        logger.warning(f'Found existing session, flushing it')
        request.session.flush()
    return JsonResponse({'text': 'OK'}, status=200)


@login_required_ajax
@require_POST
@check_post_request(['id', 'value'])
def entry(request):
    logger.info(f'Get entry update submit {request.POST}')
    response = {'text': 'OK'}
    data_pk = request.POST['id']
    try:
        data = Data.objects.get(pk=data_pk)
    except ObjectDoesNotExist:
        response['text'] = f'Data with id {data_pk} not found'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    try:
        data.set_value(request.POST['value'])
    except ValueError as error:
        response['text'] = f'Value error: {error}'
        return JsonResponse(response, status=400)
    return JsonResponse(response, status=200)


@login_required_ajax
@require_POST
@check_post_request(
    ['id', 'data', 'default_action', 'begin_with_hour', 'type'])
def scheduler_config(request):
    logger.info(f'Scheduler config submit: {request.POST}')
    response = {'text': 'OK'}
    try:
        data = Data.objects.get(pk=request.POST['data'])
    except ObjectDoesNotExist:
        response['text'] = f'Data {request.POST["data"]} not found'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    try:
        default_action = Action.objects.get(pk=request.POST['default_action'])
    except ObjectDoesNotExist:
        response['text'] = f'Action {request.POST["default_action"]} not found'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    scheduler = Scheduler.objects.get_or_create(pk=request.POST['id'])[0]
    scheduler.data = data
    scheduler.default_action = default_action
    scheduler.begin_with_hour = request.POST['begin_with_hour']
    scheduler.type = request.POST['type']
    try:
        scheduler.save()
    except Exception as error:
        response['text'] = f'Scheduler update/creation failed : {error}'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    else:
        return JsonResponse(response, status=200)


@login_required_ajax
@require_POST
@check_post_request(['id', 'scheduler', 'action', 'day_of_week', 'start_hour',
                     'start_minute', 'end_hour', 'end_minute', 'delete'])
def scheduler_event(request):
    logger.info(f'Scheduler event submit: {request.POST}')
    response = {'text': 'OK'}
    try:
        scheduler = Scheduler.objects.get(pk=request.POST['scheduler'])
    except ObjectDoesNotExist:
        response['text'] = f'Scheduler {request.POST["scheduler"]} not found'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    try:
        action = Action.objects.get(pk=request.POST['action'])
    except ObjectDoesNotExist:
        response['text'] = f'Action {request.POST["action"]} not found'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    event_pk = None
    try:
        event = scheduler.event.get(pk=request.POST['id'])
    except ObjectDoesNotExist:
        logger.info('Event is new')
    else:
        event_pk = event.pk
        event.delete()
    if request.POST['delete'] == 'true':
        return JsonResponse(response, status=200)
    try:
        event = SchedulerEvent(
            scheduler=scheduler, action=action,
            day_of_week=f'{request.POST["day_of_week"]}',
            start_hour=f'{request.POST["start_hour"]}',
            start_minute=f'{request.POST["start_minute"]}',
            end_hour=f'{request.POST["end_hour"]}',
            end_minute=f'{request.POST["end_minute"]}')
        if event_pk:
            event.pk = event_pk
        event.save()
    except Exception as error:
        response['text'] = f'Event creation failed : {error}'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    else:
        return JsonResponse(response, status=200)


@login_required_ajax
@require_POST
@check_post_request(['id', 'data', 'datachild_map'])
def control_save(request):
    control = None
    response = {'text': 'OK'}

    def get_data(data_pk):
        data_filter = Data.objects.filter(pk=data_pk)
        if not data_filter.count():
            return None, {'text': f'Unknown data with pk {data_pk}'}
        return data_filter.first(), {'text': 'OK'}

    try:
        datachild_map = json.loads(request.POST['datachild_map'])
    except json.JSONDecodeError:
        response['text'] = f'Save control: json decode error'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    if request.POST['id'] != 'new':
        control_filter = Control.objects.filter(pk=request.POST['id'])
        if control_filter.count():
            control = control_filter.first()
    known_child_map = []
    if control:
        if control.data.pk != int(request.POST['data']):
            logger.info('Control have new main data, saving model')
            main_data, response = get_data(request.POST['data'])
            if not main_data:
                logger.error(response['text'])
                return JsonResponse(response, status=400)
            control.data = main_data
            control.save()
        if hasattr(control, 'child'):
            for child in control.child.all():
                if child.data.pk in datachild_map:
                    known_child_map.append(child.data.pk)
                else:
                    child.delete()
    elif request.POST['id'] == 'new':
        logger.info('Control is new, creating it')
        main_data, response = get_data(request.POST['data'])
        if not main_data:
            logger.error(response['text'])
            return JsonResponse(response, status=400)
        control = Control.objects.create(data=main_data)
    else:
        response['text'] = f'Control {request.POST["id"]} save: not found'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    for data_pk in datachild_map:
        if data_pk in known_child_map:
            continue
        child_data, error = get_data(data_pk)
        if not child_data:
            response['text'] = f'Control child creation failed : {error}'
            logger.error(response['text'])
            return JsonResponse(response, status=400)
        Child.objects.create(control=control, data=child_data)
    return JsonResponse(response, status=200)


@login_required_ajax
@require_POST
@check_post_request(['id'])
def control_delete(request):
    logger.info(f'Control delete: {request.POST}')
    response = {'text': 'OK'}
    try:
        control = Control.objects.get(pk=request.POST['id'])
        control.delete()
    except Exception as error:
        response['text'] = f'Control delete failed: {error}'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    return JsonResponse(response, status=200)


def set_positions(db_model, request):
    response = {'text': 'OK'}
    model_name = str(db_model).split('.')[-1].replace("'>", "")
    logger.info(f'{model_name} positions submit: {request.POST}')
    try:
        positions = json.loads(request.POST['positions'])
    except json.JSONDecodeError:
        response['text'] = f'Set {model_name} positions : json decode error'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    # First check all ids exists
    for item in positions:
        if not db_model.objects.filter(pk=item['id']).count():
            response['text'] = f'Set {model_name}_{item["id"]} positions fail'
            logger.error(response['text'])
            return JsonResponse(response, status=400)
    for item in positions:
        model_filter = db_model.objects.filter(pk=item['id'])
        try:
            model_filter.update(position=item['position'])
        except Exception as error:
            response['text'] = f'Set {model_name} positions : db error {error}'
            logger.error(response['text'])
            return JsonResponse(response, status=400)
    return JsonResponse(response, status=200)


@login_required_ajax
@require_POST
@check_post_request(['positions'])
def control_positions(request):
    return set_positions(Control, request)


@login_required_ajax
@require_POST
@check_post_request(['positions'])
def chart_positions(request):
    return set_positions(Chart, request)


@login_required_ajax
@require_POST
@check_post_request(['data_pk'])
def chart_add(request):
    logger.info(f'Chart add: {request.POST}')
    data_pk = request.POST['data_pk']
    response = {'text': 'OK'}
    model_filter = Data.objects.filter(pk=data_pk)
    if not model_filter:
        response['text'] = f'Chart add failed: no data with pk {data_pk}'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    data = model_filter.first()
    if hasattr(data, 'chart'):
        response['text'] = f'Chart add failed: {data.name} chart exist'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    try:
        Chart.objects.create(data=data)
    except Exception as error:
        response['text'] = f'Chart db creation failed: {error}'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    return JsonResponse(response, status=200)


@login_required_ajax
@require_POST
@check_post_request(['chart_pk'])
def chart_remove(request):
    logger.info(f'Chart remove: {request.POST}')
    chart_pk = request.POST['chart_pk']
    response = {'text': 'OK'}
    model_filter = Chart.objects.filter(pk=chart_pk)
    if not model_filter:
        response['text'] = f'Chart {chart_pk} not found'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    chart = model_filter.first()
    try:
        chart.delete()
    except Exception as error:
        response['text'] = f'Chart delete failed: {error}'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    return JsonResponse(response, status=200)


@login_required_ajax
@require_POST
@check_post_request(['chart_pk', 'height'])
def chart_height(request):
    logger.info(f'Chart height: {request.POST}')
    chart_pk = request.POST['chart_pk']
    response = {'text': 'OK'}
    model_filter = Chart.objects.filter(pk=chart_pk)
    if not model_filter:
        response['text'] = f'Chart {chart_pk} not found'
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    try:
        model_filter.update(height=request.POST['height'])
    except Exception as error:
        response['text'] = (
            f'Chart save failed for {model_filter[0].data.name}: {error}')
        logger.error(response['text'])
        return JsonResponse(response, status=400)
    return JsonResponse(response, status=200)


@login_required_ajax
@require_POST
@check_post_request(['error'])
def test(request):
    logger.info(f'Test: {request.POST}')
    error = json.loads(request.POST['error'])
    if error:
        return JsonResponse({'text': 'Got error'}, status=400)
    return JsonResponse({'text': 'OK'}, status=200)
