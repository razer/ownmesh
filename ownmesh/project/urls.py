from django.conf.urls import include, url
from django.contrib import admin
# from frontend import urls as frontend_urls

urlpatterns = [
    url(r'', include('frontend.urls')),
    url(r'^api/admin/', admin.site.urls),
]

# urlpatterns = [
#     # url(r'', include('frontend.urls', namespace="frontend")),
#     url(r'', include('frontend.urls')),
#     url(r'^api/admin/', include(admin.site.urls)),
# ]
