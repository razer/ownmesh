from django.db import models
from django.forms import Textarea
from django.contrib.admin import AdminSite, ModelAdmin
from django.utils.translation import ugettext_lazy as _


class RulesAdmin(ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(
                           attrs={'rows': 1,
                                  'cols': 40,
                                  'style': 'height: 1.3em;'})},
    }


AdminSite.site_title = _('Ownmesh Admin')

AdminSite.site_header = _('Ownmesh Admin')

AdminSite.index_title = _('Ownmesh Admin')
