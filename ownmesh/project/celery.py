from __future__ import absolute_import
import os
import logging

from celery import Celery
from django.conf import settings as S

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')

app = Celery(S.CELERY_APP)
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: S.INSTALLED_APPS)
app.autodiscover_tasks(lambda: S.INSTALLED_APPS, related_name='connector')

logger = logging.getLogger(__name__)


@app.task(bind=True)
def debug_task(self):
    logger.info('Celery debug task !')
    return 'Request: {0!r}'.format(self.request)
