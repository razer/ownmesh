import logging
# from importlib import import_module
# import pytz
from django.conf import settings
from django.contrib.sessions.middleware import SessionMiddleware
# from django.utils import timezone
# from django.utils.deprecation import MiddlewareMixin

logger = logging.getLogger(__name__)


# class TimezoneMiddleware(MiddlewareMixin):
#
#     def process_request(self, request):
#         tzname = request.session.get('django_timezone')
#         if tzname:
#             timezone.activate(pytz.timezone(tzname))
#         else:
#             timezone.deactivate()


# class CustomSessionMiddleware(SessionMiddleware):
#
#     def process_request(self, request):
#         # logger.info('CustomSession running now')
#         engine = import_module(settings.SESSION_ENGINE)
#         session_key = request.COOKIES.get(settings.SESSION_COOKIE_NAME, None)
#         request.session = engine.SessionStore(session_key)
#         if not request.session.exists(request.session.session_key):
#             request.session.create()


class HeaderCookieSessionMiddleware(SessionMiddleware):

    def process_request(self, request):
        session_cookie = settings.SESSION_COOKIE_NAME
        header_session_cookie = f'HTTP_{session_cookie.upper()}'
        if session_cookie not in request.COOKIES:
            if header_session_cookie in request.META:
                # logger.info('Found SESSION KEY to inject as cookie')
                request.COOKIES[session_cookie] = request.META[
                    header_session_cookie]

        csrf_cookie = settings.CSRF_COOKIE_NAME
        header_csrf_cookie = f'HTTP_X_{csrf_cookie.upper()}'
        if csrf_cookie not in request.COOKIES:
            if header_csrf_cookie in request.META:
                # logger.info('Found CSRF to inject as cookie')
                request.COOKIES[csrf_cookie] = request.META[header_csrf_cookie]
