from __future__ import absolute_import

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
# from api.celery import fgApp as celery_api
# from services.celery import bgApp as celery_services
from .celery import app as celery_app
from . import admin
