"""
Django settings for project Ownmesh.

"""

import os
import locale
from tzlocal import get_localzone
from django.utils.timezone import get_current_timezone
from corsheaders.defaults import default_headers

def localtime(t):
    return t.astimezone(get_current_timezone())  # @IgnorePep8


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STATIC_ROOT = 'frontend/static/'
STATIC_DIR = os.path.join(BASE_DIR, STATIC_ROOT)
STATIC_URL = '/omstatic/'
MAIN_URL = '/'

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ''

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_celery_beat',
    'django_extensions',
    'api',
    'core',
    'core.hosts',
    'core.datas',
    'core.actions',
    'core.hosts.local',
    'core.hosts.local.gpio',
    'core.hosts.xbee',
    'core.hosts.serial',
    'core.hosts.tcpsocket',
    'core.hosts.rf24',
    'frontend',
    'frontend.controls',
    'frontend.events',
    'frontend.charts',
    'django.contrib.admin',
    'channels',
    'corsheaders',
]

# MIDDLEWARE_CLASSES = [
MIDDLEWARE = [
    'project.middleware.HeaderCookieSessionMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = False
CORS_ALLOW_CREDENTIALS = True
CORS_ALLOW_HEADERS = default_headers + ('sessionid',)
# CORS_ORIGIN_WHITELIST = ()
# CSRF_TRUSTED_ORIGINS = ()

ROOT_URLCONF = 'project.urls'
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': False,
            'context_processors': (
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ),
        },
    },
]

DATABASES = {
    'default': {
        'NAME': 'ownmesh',
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'USER': 'ownmesh',
        'PASSWORD': 'k125tc3',
        'CONN_MAX_AGE': 600,
    }
}

# SOUTH_TESTS_MIGRATE = False
# if 'test' in sys.argv:
#     DATABASES['default'] = {'ENGINE': 'django.db.backends.sqlite3'}

dca = 'django.contrib.auth.'
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': dca + 'password_validation.UserAttributeSimilarityValidator', },
    {'NAME': dca + 'password_validation.MinimumLengthValidator', },
    {'NAME': dca + 'password_validation.CommonPasswordValidator', },
    {'NAME': dca + 'password_validation.NumericPasswordValidator', },
]

LOGGING_BASEPATH = '/var/log/ownmesh'
PIDFILE_BASEPATH = '/var/run/ownmesh'

REDIS_SOCK = '/run/redis/redis.sock'
DAPHNE_SOCK = os.path.join(PIDFILE_BASEPATH, 'daphne.sock')

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            # 'hosts': [('localhost', 6379)],
            'hosts': [f'unix://{REDIS_SOCK}'],
        },
    },
}
ASGI_APPLICATION = 'project.routing.application'

LANGUAGE_CODE = locale.getdefaultlocale()[0].replace('_', '-')
TIME_ZONE = get_localzone().zone
# TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

WSGI_APPLICATION = 'project.wsgi.application'

CELERY_APP = 'project'

# Redis TCP
# CELERY_BROKER_URL = 'redis://localhost:6379/0'
# CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

# Redis Unix sock
CELERY_BROKER_URL = 'redis+socket:///run/redis/redis.sock'
CELERY_RESULT_BACKEND = 'redis+socket:///run/redis/redis.sock'

CELERY_BROKER_TRANSPORT_OPTIONS = {'fanout_prefix': True}
CELERY_BACKGROUND_QUEUE = 'services'
CELERY_BACKGROUND_LOG = os.path.join(
    LOGGING_BASEPATH, 'celery-%s.log' % CELERY_BACKGROUND_QUEUE)
CELERY_BACKGROUND_LOGLEVEL = 'INFO'
CELERY_BACKGROUND_CONCURRENCY = '1'
CELERY_BACKGROUND_PIDFILE = os.path.join(
    PIDFILE_BASEPATH, 'celery-%s.pid' % CELERY_BACKGROUND_QUEUE)

CELERY_RESULT_SERIALIZER = 'pickle'
CELERY_RESULT_PERSISTENT = False
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_ACCEPT_CONTENT = ['pickle']
CELERY_WORKER_CONCURRENCY = '1'
CELERY_BEAT_CONCURRENCY = '1'
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'
CELERY_BEAT_LOG = os.path.join(LOGGING_BASEPATH, 'celery-scheduler.log')
CELERY_BEAT_PIDFILE = os.path.join(PIDFILE_BASEPATH, 'celery-scheduler.pid')
CELERY_BEAT_LOGLEVEL = 'INFO'
# CELERY_TIMEZONE = TIME_ZONE
# CELERY_ENABLE_UTC = False

# Hosts plugins
GPIO_SOCK = os.path.join(PIDFILE_BASEPATH, 'gpio.sock')
HOST_SOCK = os.path.join(PIDFILE_BASEPATH, 'host.sock')

log_verbose = '[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s'
max_size = 1024 * 1024 * 5  # 5 MB
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': log_verbose,
            'datefmt': "%d/%m %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'djangoLogFile': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOGGING_BASEPATH, 'django.log'),
            'maxBytes': max_size,
            'backupCount': 3,
            'formatter': 'verbose'
        },
        'base_logfile': {
            'level': CELERY_BACKGROUND_LOGLEVEL,
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': CELERY_BACKGROUND_LOG,
            'maxBytes': max_size,
            'backupCount': 3,
            'formatter': 'verbose'
        },
        'restapi_logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOGGING_BASEPATH, 'rest-api.log'),
            'maxBytes': max_size,
            'backupCount': 3,
            'formatter': 'verbose'
        },
        'errors_logfile': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': CELERY_BACKGROUND_LOG,
            'maxBytes': max_size,
            'backupCount': 3,
            'formatter': 'verbose'
        },
        'scheduler_logfile': {
            'level': CELERY_BEAT_LOGLEVEL,
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': CELERY_BEAT_LOG,
            'maxBytes': max_size,
            'backupCount': 3,
            'formatter': 'verbose'
        },
        'gpio_logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOGGING_BASEPATH, 'gpio.log'),
            'maxBytes': max_size,
            'backupCount': 3,
            'formatter': 'verbose'
        },
    },
    'loggers': {
        # Things to get rid of totaly
        'kombu': {
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'tornado': {
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': False,
        },

        # Only errors
        'celery.worker.job': {
            'handlers': ['errors_logfile', 'null'],
            'level': 'DEBUG',
            'propagate': False,
        },

        # Project specific
        'asyncio': {
            'handlers': ['base_logfile', 'null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'django': {
            'handlers': ['base_logfile', 'null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'project': {
            'handlers': ['base_logfile', 'null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'frontend': {
            'handlers': ['base_logfile', 'null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'core.hosts.local': {
            'handlers': ['gpio_logfile', 'null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'core': {
            'handlers': ['base_logfile', 'null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'celery.beat': {
            'handlers': ['scheduler_logfile', 'null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'api': {
            'handlers': ['restapi_logfile', 'null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'celery': {
            'handlers': ['base_logfile', 'null'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}
