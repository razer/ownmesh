# IO='IO'
BOOL = 'BO'
INT = 'IT'
FLOAT = 'FL'
STRING = 'ST'
TYPE_CHOICES = (
    (BOOL, 'Boolean'),
    (INT, 'Integer'),
    (FLOAT, 'Float'),
    (STRING, 'String')
)

READONLY = 'RO'
WRITEONLY = 'WO'
READWRITE = 'RW'
MODE_CHOICES = (
    (READONLY, 'Read only (from host)'),
    (WRITEONLY, 'Write only (remote command'),
    (READWRITE, 'Read/Write'),
)

NONE = 'NO'
NETSTATUS = 'NS'
ALARM = 'AL'
THRESHOLD = 'TR'
ROLE_CHOICES = (
    (NONE, 'No role'),
    (NETSTATUS, 'Host network status'),
    (ALARM, 'Alarm data'),
    (THRESHOLD, 'Alarm threshold'),
)

ALWAYS = 'AL'
ONLYNEWVALUE = 'NV'
NOPERSISTANCE = 'EP'
ENTRY_CHOICES = (
    (ALWAYS, 'Always create entry'),
    (ONLYNEWVALUE, 'Create entry when value change'),
    (NOPERSISTANCE, 'Always edit previous entry'),
)

NONE = 'NO'
BELOW = 'BE'
UPPER = 'UP'
CHILDUSAGE_CHOICES = (
    (NONE, 'None'),
    (BELOW, 'Alarm below limit'),
    (UPPER, 'Alarm upper limit'),
)

RECEIVED = 'RE'
TRANSMITTED = 'TR'
REMOTE_CHOICES = (
    (RECEIVED, 'Received'),
    (TRANSMITTED, 'Transmitted'),
)
