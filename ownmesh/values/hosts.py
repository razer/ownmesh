from django.utils.translation import ugettext_lazy as _

# REMOTECONF_KEYS = {'NAME':    _('Name :'),
#                    'TYPE':    _('Type :'),
#                    'COLOR':   _('Color :'),
#                    'PORT':    _('Port'),
#                    'ADDRESS': _('Address :'),
#                    'IP':      _('IP address :'),
#                    'VISIBLE':  _('Visible :')
#                    }

GUICOLOR_CHOICES = (
    ('yel', _('yellow')),
    ('blu', _('blue')),
    ('gre', _('green')),
    ('ora', _('orange')),
    ('pur', _('purple')),
    ('bla', _('black')),
    ('gry', _('grey')),
)

STOPPED = 'ST'
RUNNING = 'RU'
FAILED = 'FA'

PLUGIN_STATE = (
    (STOPPED, _('Stopped')),
    (RUNNING, _('Running')),
    (FAILED, _('Failed'))
)

ALWAYS_ON = 'NO'
SLEEP = 'SL'
DEEP_SLEEP = 'DS'

POWERMODE_CHOICES = (
    (ALWAYS_ON, _('Always on')),
    (SLEEP, _('Wake up on request')),
    (DEEP_SLEEP, _('Deep sleep : no listening'))
)


class FRAME:
    DEBUGSEQ = b'D:'
    STARTSEQ = b'F:'
    DELIMIT = b'^'
    NEWLINE = b'\n'
    CRETURN = b'\r'

    RXOK = b'0'
    RXER = b'1'
    TXDT = b'2'
    TXRQ = b'3'
    TXVL = b'4'
    PING = b'5'
    PONG = b'6'


class RequestHeader:
    CLOCK = b'f0'
    HANDLEDDATA = b'f1'
    ALLVALUES = b'f2'
    PING = b'f3'
