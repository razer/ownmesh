SMALL = 130
MEDIUM = 280
LARGE = 500

CHARTHEIGHT_CHOICES = (
    (SMALL,     'Small'),
    (MEDIUM,    'Medium'),
    (LARGE,     'Large'),
)
