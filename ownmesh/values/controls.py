IFTRUE = 'IT'
IFFALSE = 'IF'
BELOW = 'BE'
UPPER = 'UP'

ALARMDATATYPE_CHOICES = (
    (IFTRUE,        'Alarm if True'),
    (IFFALSE,       'Alarm if False'),
    (BELOW,         'Below limit'),
    (UPPER,         'Upper limit'),
)
