SIZE_DEFAULT=15

ALL='AL'
ALARM='AM'
NETSTATUS='NT'
DATAVALUE='DT'
LAST='LA'

EVENTTYPE_CHOICES=(
    (ALL,       'All'),
    (NETSTATUS, 'Net Status'),
    (ALARM,     'Alarms'),
    (DATAVALUE, 'Data Values'),
    )

DEFAULTTYPE_CHOICES=(
    (LAST,      'Last selected'),
    (ALL,       'All'),
    (NETSTATUS, 'Net Status'),
    (ALARM,     'Alarms'),
    (DATAVALUE, 'Data Values'),
    )