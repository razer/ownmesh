# TODO: edit that !
SCHEDULERTASK = 'my_task'

SETVALUE = 'SE'
SYNC = 'SY'
COMPARE = 'CO'
CUSTOM = 'CU'
ACTIONTYPE_CHOICES = (
    (SETVALUE, 'Set data Value as argument'),
    (SYNC, 'Value sync'),
    (COMPARE, 'Compare'),
    (CUSTOM, 'Custom action'),
    )

WEEK = 'WE'
OPENDAYS = 'OD'
DAY = 'DY'
# DATE = 'DA'
ACTIONSCHEDTYPE_CHOICES = (
    (WEEK, 'Weekly'),
    (OPENDAYS, 'Open days / Weekend'),
    (DAY, 'Daily'),
    # (DATE, 'specific date'),
    )

DAYOFWEEK = [b'Sunday', b'Monday', b'Tueday', b'Wedday', b'Thuday', b'Friday',
             b'Satday']
