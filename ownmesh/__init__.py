from __future__ import absolute_import

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
# from api.celery import fgApp as celery_api
# from services.celery import bgApp as celery_services
# from core.celery import app as celery_app

import sys
import os

# Internal classes import
sys.path.append(os.path.dirname(__file__))
# sys.path.append(os.path.join(os.path.dirname(__file__),'core'))
# sys.path.append(os.path.join(os.path.dirname(__file__),'frontend'))
# sys.path.append(os.path.join(os.path.dirname(__file__),'tools'))
# sys.path.append(os.path.join(os.path.dirname(__file__),'listener'))
